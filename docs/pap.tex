\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{stmaryrd} % for semantic brackets [[ ]]
\usepackage{xspace}
\usepackage{color}
\usepackage{comment}

\usepackage{newlfont}
\usepackage{program}
\NumberProgramstrue

\def\keyword#1{\mbox{\normalshape\bf #1}}
\def\ENDPROC{\untab}
\def\FUN{\qtab\keyword{fun}\ }
\def\ENDFUN{\untab}
\def\LOOP{\qtab\keyword{loop}\ }
\def\ENDLOOP{\untab}
\def\REPEAT{\qtab\keyword{repeat}\ }
\def\UNTIL{\untab\keyword{until}\ }
\def\IF{\qtab\keyword{if}\ }
\def\THEN{\ \keyword{then}\ }
\def\ELSE{\untab\qtab\keyword{else}\ }
\def\ELSEIF{\untab\qtab\keyword{else if}\ }
\def\ENDIF{\untab}
\def\FORALL{\qtab\keyword{forall}\ }
\def\ENDFORALL{\untab}
\def\FOR{\qtab\keyword{for}\ }
\def\ENDFOR{\untab}
\def\TRY{\qtab\keyword{try}\ }
\def\CATCH{\untab\keyword{catch}\ }
\def\RETURN{\keyword{return}\ }
\def\RAISE{\keyword{raise}\ }
\def\COM#1{\textcolor{blue}{\texttt{//\;#1}}}
\def\MATCH{\qtab\keyword{match}\ }
\def\WITH{\ \keyword{with}\ }
\def\ENDMATCH{\untab}
\def\CASE{\qtab }
\def\ENDCASE{\untab}
\def\WHILE{\qtab\keyword{while}\ }
\def\DOO{\ \keyword{do}\ }
\def\ENDWHILE{\untab}
\def\OR{\ \keyword{or}\ }

\usepackage{listings}
\definecolor{listingComment}{rgb}{0.3,0.7,0.3}
\definecolor{listingString}{rgb}{0.3,0.3,0.7}
\definecolor{listingBackground}{rgb}{0.95,0.95,0.95}
\lstdefinestyle{lustre}
    { basicstyle=\scriptsize\sffamily,
      morekeywords={let, tel, returns, implies, var, node, int, bool, if, then ,else, PROPERTY, or, pre, and},
      showstringspaces=false,
      numbers=left,
      numbersep=1pt,
      tabsize=6,
      backgroundcolor=\color{white},
      keywordstyle={[2]\color{black}\bfseries},
      stringstyle=\mdseries\slshape\color{listingString},
      commentstyle=\itshape\color{listingComment},
      emphstyle={\itshape},
      emphstyle={[2]\color{red}},
      emphstyle={[3]\color{blue}\bfseries},
      emphstyle={[4]\color{blue}},
      mathescape=true
    }
\lstnewenvironment{lustre}
  {\lstset{style=lustre}}{}

\lstdefinestyle{debug}
    { basicstyle=\scriptsize\sffamily,
      morekeywords={pop, push, assert, check, SAT, UNSAT, define, int, bool, ite, subrange, true, false},
      showstringspaces=false,
      numbers=left,
      numbersep=1pt,
      tabsize=6,
      backgroundcolor=\color{white},
      keywordstyle={[2]\color{black}\bfseries},
      stringstyle=\mdseries\slshape\color{listingString},
      commentstyle=\itshape\color{listingComment},
      emphstyle={\itshape},
      emphstyle={[2]\color{red}},
      emphstyle={[3]\color{blue}\bfseries},
      emphstyle={[4]\color{blue}},
      mathescape=true
    }
\lstnewenvironment{debug}
  {\lstset{style=debug}}{}

%\usepackage{todonotes}
%\newcommand{\tk}[1]{\todo[size=\small, color=red!40]{TK: #1}}
%\newcommand{\ct}[1]{\todo[size=\small, color=green!40]{CT: #1}}

\newcommand{\rem}[1]{\textsf{\small [#1]}}
\newcommand{\s}{q}
%\newcommand{\iset}{\Sigma_\mathrm{I}}
\newcommand{\sspace}{\mathbf{Q}}
\newcommand{\iset}{\mathbf{I}}
\newcommand{\trel}{\leadsto}
\newcommand{\ff}{\mathsf{false}}
\newcommand{\limp}{\Rightarrow}
\newcommand{\wpf}{\wp_\mathrm{f}}
\newcommand{\E}{\mathbf{E}}
%\newcommand{\A}{\mathbf{A}}
\newcommand{\Mo}{\mathbf{M}}
\newcommand{\Fo}{\mathbf{F}}

\newcommand{\lo}{\ensuremath{\mathcal{L}}\xspace}
\newcommand{\ill}{\ensuremath{\mathcal{IL}}\xspace}
\newcommand{\D}{\ensuremath{\mathbb{D}}\xspace}
\newcommand{\ps}{\ensuremath{\mathcal{P}}\xspace}
\newcommand{\sys}{\ensuremath{\mathcal{S}}\xspace}
\newcommand{\M}{\ensuremath{\mathcal{M}}\xspace}
\newcommand{\lent}{\,\models_\lo\,}
\newcommand{\ini}[1]{{#1}_\mathrm{I}}
\newcommand{\tra}[1]{{#1}_\mathrm{T}}

\newcommand{\ext}[1]{\llbracket #1 \rrbracket}
\newcommand{\defas}{\stackrel{\mathrm{def}}{=}}

\newcommand{\I}{\mathcal{M}}

\newcommand{\co}[1]{\overline{#1}}

\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\preds}{\mathbf{P}}
\newcommand{\avars}{\mathbf{A}}
\newcommand{\bvars}{\mathbf{N}}
\newcommand{\bexists}{\exists_\bvars}

\newcommand{\define}[1]{\textcolor{blue}{\emph{#1}}}


\title{Invariant Generation with BDDs}
%\author{Clark Barrett and Temesghen Kahsai and Cesare Tinelli}
%\date{} 
\begin{document}
\maketitle
\begin{abstract}
A procedure to generate invariants that are arbitrary Boolean combinations of given predicates.
\end{abstract}


\section{Preliminaries}

\paragraph{Transition Systems.}
We model computational systems as transition systems.
A \define{transition system} $S$
is a triple
$(\sspace, \iset, \trel)$ where 
$\sspace$ is a set of \define{states}, the \define{state space};
$\iset \subseteq \sspace$ is the set of $S$'s \define{initial states}; and
$\trel\ \subseteq \sspace \times \sspace$ is $S$'s \define{transition relation}. 
%
A state $\s \in \sspace$ is \define{$i$-reachable} for $i \geq 0$
if $\s \in \iset$,
or $\s' \trel \s$ for some $i'$-reachable state $\s'$ with $i' < i$.
A state $\s \in \sspace$ is \define{reachable} if it is $i$-reachable for some $i$.


\paragraph{Logic.}
We assume a sublogic $\lo = (\Sigma, \Fo, \Mo)$ 
of many-sorted first-order logic with equality
with a decidable entailment relation $\lent$ and a language $\Fo$ closed 
under all the Boolean operators and quantifiers.

If $F$ is a $\Sigma$-formula and $(x_1,\ldots,x_n)$ a tuple of distinct variables,
we write $F[x_1,\ldots,x_n]$ to express that
the free variables of $F$ are in $(x_1,\ldots,x_n)$;
furthermore, if $t_1,\ldots,t_n$ are terms with each $t_i$ of the same sort 
as $x_i$, we write
$F[t_1,\ldots,t_n]$ to denote the formula obtained from $F[x_1,\ldots,x_n]$ by
simultaneously replacing each occurrence of $x_i$ in $F$ by $t_i$, 
for $i=1,\ldots, k$.


For each sort $\sigma$ in $\lo$, we distinguish a set $V_\sigma$ 
of variable-free terms, 
which we call \define{values}, such that 
$\lent \lnot(v_1 = v_2)$ for each distinct $v_1, v_2 \in V_\sigma$.
%Examples of values would be integer constants, 
%selected terms of the form $n/m$ where $n$ is an integer constant and 
%$m$ a non-zero numeral, and so on.
We assume that the satisfiable formulas of \lo are \define{satisfied by values}, 
that is, for every formula $F[\vec y]$ (with free variables from $\vec y$) satisfied by a model $\I$ of \lo
there is a value tuple $\vec v$ such that $F[\vec v]$ is satisfied by $\I$.\footnote{
LIA satisfies this assumption.
}


We assume a total surjective encoding of $S$'s state space $\sspace$ 
to $n$-tuples of values, for some fixed $n$
where each $n$-tuple encodes a state.
%Depending on \lo, states may be encoded, for instance,  
%as tuples of Boolean constants, or integer constants, 
%or mixed tuples of Boolean, integer and rational constants,
%and so on.
Because of this encoding \emph{we identify states with tuples of values}.
Note that, thanks to our various assumptions,
each formula $F[\vec y_1, \ldots,\vec y_k]$ of \lo with $k\cdot n$
free variables denotes a subset of $\sspace^k$,
namely the set of all $k$-tuples of states that satisfy $F$. 
Note that, thanks to our various assumptions,
each formula $F[\vec y_1, \ldots,\vec y_k]$ in $k\cdot n$ variables denotes 
a subset of $\sspace^k$,
namely the set of all $k$-tuples of states that satisfy $F$. 
We call that set the \define{extension} of $F$ and define it formally as follows:
\[
 \ext{F} \ \defas \ 
 \{(\vec v_1, \ldots,\vec v_k) \in \sspace^k \mid 
   F[\vec v_1, \ldots,\vec v_k] \text{ is satisfiable in } \lo
 \} \ .
\]
We refer to formulas like $F$ above as \define{state formulas} and 
say they are \define{satisfied} by the state sequences in $\ext{F}$.

\paragraph{Logical encodings of transition systems.}
Finally, we assume the existence of an \define{encoding of $S$ in \lo},
a pair
\[ 
 (I[\vec x],\, T[\vec x,\vec x'])
\]
of \emph{quantifier-free} state formulas, 
where
$I[\vec x]$ is satisfied exactly by the initial states of $S$, and
$T[\vec x,\vec x']$ is satisfied
by two reachable states $\vec v,\vec v'$ iff $ \vec v \trel \vec v'$.
\smallskip

A (single) state formula $P[\vec x]$ of \lo is \define{$k$-invariant (for $S$)}
for some $k \geq 0$ if it is satisfied by all $k$-reachable states of $S$.
$P[\vec x]$ is \define{invariant (for $S$)} 
if it is satisfied by all reachable states of $S$.

\section{Generating predicates}

Possible of generating predicates:

\begin{description}
\item[From mode variables]: 

\item[Guards in if-then conditions]:

\end{description}

\section{Algorithm 1}

Let $\preds = \{P_1[\vec x], \ldots, P_m[\vec x]\}$ be 
a set of atomic state formulas that we call \define{predicates}.
Our goal is to generate a quantifier-free formula over $\vec x$ 
that over-approx\-imates the conjunction of all invariants 
expressible as a Boolean combinations of predicates in $\preds$.
Roughly, we do that by finding and storing such combinations in a BDDs.
The over-approximation comes from the fact that, for efficiently, 
some BDD operations might not be exact.

We will use the following.
\begin{enumerate}
\item
A set $\avars = \{A_1, \ldots, A_m\}$ of Boolean variables.

\item
A function $\alpha$ from Boolean combinations of predicates in $\preds$
to BDDs over $\avars$.
For every $F$, $\alpha(F)$ is a BDD of the Boolean abstraction of $F$ obtained 
by replacing each predicate $P_i$ by $A_i$.

\item
A function $\gamma$ from BDDs over $\avars$ 
to Boolean combinations of predicates in $\preds$
such that 
$\alpha(\gamma(D))$ is a BDD propositionally equivalent to $B$.
\rem{
Concretely, $\gamma$ can be implemented by first turning $D$ into 
an equivalent propositional formula $F$ over $\avars$ and instantiating 
each $A_i$ in $F$ with $P_i$.
}

\end{enumerate}

The invariant generation procedure is described by the pseudocode function |gen_inv| below.
\bigskip

\begin{programbox}
\COM{Ensures:~the returned formula is invariant}
\FUN |gen_inv| \BODY
 D_0 := \alpha(\bot)
 \WHILE I[\vec x] \land \lnot \gamma(D_0)[\vec x] \not\lent \bot
  \COM{some initial states are not covered by $\gamma(D_0)$}
  |let | \vec s \in \ext{I[\vec x] \land \lnot \gamma(D_0)[\vec x]}
  D_0 := |join|(D_0, |cube|\ \vec s)
  \COM{All states with same abstraction as $\vec s$ are in $\ext{\gamma(D_0)}$}
 \ENDWHILE
 \COM{$I[\vec x] \lent \gamma(D_0)[\vec x]$}
 \FOR k = 0 \TO \infty
 \COM{All $k$-reachable states are in $\ext{\gamma(D_k)}$}
  D_{k+1} := D_k
  |let | F | abbreviate | \gamma(D_k)[\vec x] \land T[\vec x, \vec x'] \land\
         \!\lnot \gamma(D_{k+1})[\vec x']
  \IF F \lent \bot \THEN
   \COM{All reachable states are in $\ext{\gamma(D_k)}$}
   \RETURN \gamma(D_k)[\vec x]
  \ELSE
   \REPEAT
    |let | (\vec s, \vec s') \in \ext{F}
    D_{k+1} := |join|(D_{k+1}, |cube|\ \vec s')
  \UNTIL F \lent \bot
  \COM{$\gamma(D_k)[\vec x] \land T[\vec x, \vec x'] \lent \gamma(D_{k+1})[\vec x']$}
  \ENDIF
 \ENDFOR
\ENDFUN
\end{programbox}
\bigskip

\begin{programbox}
\COM{Assumes:~$\vec s$ is a state}
\COM{Ensures:~the returned value is a BDD over $\avars$}
\COM{\hspace{4em} abstracting $\vec s$}
\FUN |cube|\ \vec s \BODY
 \FOR i = 1 \TO m
  \IF \;\vec x = \vec s \lent P_i[\vec x]\; \THEN
   L_i := P_i
  \ELSE
   L_i := \lnot P_i
  \ENDIF
 \ENDFOR
 \RETURN \alpha(L_1 \land \cdots \land L_m)
\ENDFUN
\end{programbox}
\bigskip

\begin{programbox}
\COM{Assumes:~$D_1, D_2$ BDDs over $\avars$}
\COM{Ensures:~returned BDDs over-approximates}
\COM{\hspace{4em} the disjunction of $D_1$ and $D_2$} 
\FUN |join|\ (D_1, D_2)
\ENDFUN
\end{programbox}
\bigskip

\rem{The function |join| above will be implemented using the standard join and
the approximation algorithm in \cite{RaviEtAl-DAC-98}.}

\begin{comment}
\section{Algorithm Improved}

This version of the algorithm improved on the previous one
by translating the BDD into a more compact formula
that relies on the use of additional Boolean variables.

The generated invariant $F[\vec x]$ might not be quantifier free in this case, 
but it will contain at most existential quantifiers, for the additional
Boolean variables.
Hence it can be used to strengthen induction arguments with no problems 
because such strengthening occurs on the left side of entailment queries.
For uses of $F$ on the right side of an entailment it will be necessary
to eliminate the existential quantifiers first.
However, $F$ will be constructed so that this elimination can be achieved
by resolution, possibly at the cost of an explosion in the size of the resulting formula.


Informally, the main idea is the following.
To start, observe that the Boolean function encoded in a BDD $D$ can be 
equivalently represented by a set of clauses
where each clause is negation of the cube corresponding to a path 
from $D$'s root to the false node.
For instance, if 
\[
 A_1 \stackrel{0}{\longrightarrow} \
 A_2 \stackrel{0}{\longrightarrow} \
 A_3 \stackrel{1}{\longrightarrow} \
 0
 \text{ \ and \ } 
 A_1 \stackrel{0}{\longrightarrow} \
 A_2 \stackrel{1}{\longrightarrow} \
 A_4 \stackrel{0}{\longrightarrow} \
 0
\]
are two such paths, the corresponding clauses will be respectively
\[
 A_1 \lor A_2 \lor \co A_3
 \text{ \ and \ } 
 A_1 \lor \co A_2 \lor A_4\ .
\]
To avoid the exponential explosion of clauses obtaining by tracing 
each path BDD path individually, 
we add auxiliary variables as stand-ins for the BDD nodes and 
produce at most ternary clauses making sure that clauses along 
a shared path prefix or suffix are repeated only once.

For instance, the paths above are translated into the clause set
\[
 \begin{array}{l@{\;\,}l}
  N_1,\; \co N_1 \lor \co P_1 \lor N_2,
  & 
  \co N_2 \lor \co P_2 \lor N_3,\;
  \co N_3 \lor P_3,
  \\
  & \co N_2 \lor P_2 \lor N_4,\;
    \co N_4 \lor \co P_4
 \end{array}
\]
where each $P_i$ is the predicate abstracted by $A_i$
and each $N_i$ represents the BDD node storing $A_i$.
In general,
\begin{itemize}
\item
a ternary clause of the form $\co N_i \lor P_i \lor N_j$ encodes 
the link between a non-leaf node with variable $A_i$ and its high child,
\item
a clause of the form $\co N_i \lor \co P_i \lor N_k$ encodes 
the link between the same node and its low child.
\end{itemize}
Similarly, binary clauses represent links between non-leaf nodes and 
the false node.

A later optimization pass can reduce the number of clauses by eliminating,
by resolution, auxiliary variables that occur exactly once in each polarity
in the whole set.
For instance, the subset of clauses above can be compacted into
\[
 \begin{array}{l@{\;}l}
  P_1 \lor N_2,\;
  & 
  \co N_2 \lor \co P_2 \lor \co P_3,\;
  \\
  & \co N_2 \lor P_2 \lor \co P_4
 \end{array}
\]
if $P_3$ and $P_4$ do not occur in other paths to the false node.
\rem{
A smarter translation procedure would produce the compacted clause set
directly without the need for a second pass.
}



Let $\preds, \avars, \alpha$ and $\gamma$ be defined 
as in the previous section.
Let $\bvars = \{N_i\}_i$ be a set of \emph{Boolean} variables disjoint 
from $\preds$.

Define as sketched above a function $\hat{\gamma}$ from BDDs over $\avars$ 
to Boolean combinations of predicates in $\preds \cup \bvars$
such that 
$\gamma(D)$ is in CNF and is logically equivalent 
to $\exists_\bvars\, \hat{\gamma}(D)$
where $\exists_\bvars$ denotes the existential quantification 
of all the variables of $\bvars$ that occur in $\hat{\gamma}(D)$.

The improved invariant generation procedure is defined as follows.

\begin{programbox}
\COM{Ensures:~returned formula is invariant}
\FUN |gen_inv| \BODY
 D_0 := \alpha(\bot)
 \WHILE I \land \bexists\,\hat{\gamma}(|not|\ D_0) \not\lent \bot
  \COM{some initial states are not covered by $\hat\gamma(D_0)$}
  \COM{(equivalently, $\gamma(D_0)$)}
  |let | \vec s \in \ext{I \land \bexists\, \hat{\gamma}(|not|\ D_0)}
  D_0 := |join|(D_0, |cube|\ \vec s)
  \COM{All states with same abstraction as $\vec s$ are}
  \COM{in $\ext{\gamma(D_0)}$}
 \ENDWHILE
 \COM{$I \lent \gamma(D_0)$}
 \FOR k = 0 \TO \infty
  \COM{All $k$-reachable states are in $\ext{\gamma(D_k)}$}
  D_{k+1} := D_k
  |let | F | abbreviate|\
  \bexists\,\hat{\gamma}(D_k)[\vec x] \land T[\vec x, \vec x'] \land\
  \bexists\,\hat{\gamma}(|not|\ D_{k+1})[\vec x']
 \IF F \lent \bot \THEN
   \COM{All reachable states are in $\ext{\gamma(D_k)}$}
   \RETURN \bexists\,\hat{\gamma}(D_k)[\vec x]
  \ELSE
     \WHILE F \not\lent \bot
    |let | (\vec s, \vec s') \in \ext{F}
    D_{k+1} := |join|(D_{k+1}, |cube|(\vec s'))
    \ENDWHILE
    \COM{$\gamma(D_k)[\vec x] \land T[\vec x, \vec x'] \lent \gamma(D_{k+1})[\vec x']$}
  \ENDIF
 \ENDFOR
\ENDFUN
\end{programbox}
\bigskip

\begin{programbox}
\COM{Assumes:~$D$ BDDs over $\avars$}
\COM{Ensures:~returned BDD is the negation of $D$} 
\FUN |not|\ D
\ENDFUN
\end{programbox}
\bigskip

The function $\hat{\gamma}$ can be implemented by the procedure |to_formula| below
for non-trivial BDDs.
We assume a standard implementation of reduced BDDs,
with functions |low| and |high| returning respectively the low and the high sub-node
of a given non-leaf node of the BDD,
as well as Boolean functions |is_zero| and |is_one| over nodes, 
with the expected behavior.
The implementation uses a (mutable) hash table to keep track of already visited 
sub-BDDs so that they are not converted more than once.
If $D$ is the root of a BDD,
$(|to_formula | D)$ returns a list of the clauses 
whose conjunction is equisatisfiable with $\gamma(D)$.

\newcommand{\vis}{\mathit{vis}}

\begin{programbox}
\COM{Assumes:~$D$ is the root node of a non-trivial BDD}
\COM{Ensures:~returned value is a list of clauses that }
\COM{\hspace{4em} together are equisatisfiable with $\gamma(D)$} 
\FUN |to_formula | D \BODY
  \vis := |new hash_table|
  N := |to_var | D
  P := |to_pred | D
  c_l := |to_formula_aux | D\ (|low | r)\  P\ \vis
  c_h := |to_formula_aux | D\ (|high | r)\ \lnot P\ \vis
 \RETURN N :: (|append | c_l\ c_h)
\ENDFUN
\end{programbox}
\bigskip

\begin{programbox}
\COM{Assumes:~$D_i$ is the root node of a non-trivial BDD;}
\COM{\hspace{4em} $D_j$ is a child node of $D_i$;}
\COM{\hspace{4em} $P$ is (negation of) the predicate associated}
\COM{\hspace{4em}  with $D_i$ if $D_j = |low | D_i$ ($D_j = |high | D_i$)}
\COM{Ensures:~the returned clause list has no repetitions;}
\COM{\hspace{4em} the returned clauses together with $(|to\_var | D_i)$} 
\COM{\hspace{4em} are equisatisfiable with $\gamma(D_i)$} 

\FUN |to_formula_aux | D_i\ D_j\ P_i \BODY
 \IF (|is_one | D_j) \THEN
  \RETURN |Nil|
 \ELSE
  N_i := |to_var | D_i
  N_j := |to_var | D_j
  \IF (|is_zero | D_j) \THEN
   \RETURN (\lnot N_i \lor P_i) :: |Nil|
  \ELSEIF (|find | D_j\ \vis) \THEN
   \RETURN (\lnot N_i \lor P_i \lor N_j) :: |Nil|
  \ELSE
    P_j := |to_pred | D_j
	c_l := |to_formula_aux | D_j\ (|low | D_j)\ P_j
    c_h := |to_formula_aux | D_j\ (|high | D_j)\ \lnot P_j
    |add | D_j\ vis
    \RETURN (\lnot N_i \lor P_i \lor N_j) :: (|append | c_l\ c_h)
  \ENDIF
 \ENDIF
\ENDFUN
\end{programbox}
\bigskip

\end{comment}





\section{Implementation}

Let consider the following Lustre program:

\begin{lustre}
 node test (x: bool) returns (OK: bool);
  var V:bool;
     first: subrange [1,3] of int;
     second: subrange [4,8] of int;
  let
    V = false -> (pre V or true);
    first = 1 -> pre first + 1;
    second = if (first=3) then (if V then 4 else 5) else 6 ; 
    OK = not (first >= 1) or (second<=4);
    --@ PROPERTY : OK;
  tel.
\end{lustre}

In the following I report what is printed to the SMT solver. 

1. Define the various functions:

\begin{debug}
(define base::int)
(define n:: int)
(define first ::(-> int (subrange 1 3)))	
(define second ::(-> int (subrange 4 8)))
(define OK ::(-> int bool))
(define V ::(-> int bool))
(define DEF__106::(-> int bool) (lambda ( M::int) (= (second M) 
                                (ite (= (first M) 3) (ite (= (V__4 M) true) 4 5) 6))))
(define DEF__103::(-> int bool) (lambda ( M::int) (= (OK M) (or 
                                (not (>= (first M) 1)) (<= (second M) 4)))))
(define DEF__105::(-> int bool) (lambda ( M::int) (= (first M) 
                                (ite (= M base) 1 (+ (first (- M 1)) 1)))))
(define DEF__104::(-> int bool) (lambda ( M::int) (= (V M) 
                                (ite (= M base) false (or (V (- M 1)) true)))))
\end{debug}

2. Define the candidate predicates, i.e., ``unfolded'' subrange values:

\begin{debug}
(define pred_5 ::(-> int bool) (lambda ( M:: int) (= ( second M) 4)))
...
(define pred_1 ::(-> int bool) (lambda ( M::int) (= ( second M) 8)))
(define pred_8 ::(-> int bool) (lambda ( M::int) (= ( first M) 1)))
...
(define pred_6 ::(-> int bool) (lambda ( M:: int) (= ( first M) 3)))
\end{debug}

3. Define two functions; \texttt{obs}: function to observe predicates, and \texttt{node}: utility function to describe BDD nodes.

\begin{debug}
(define obs ::(-> int bool))
(define node ::(-> int int bool))
\end{debug}

4. We are now asserting the formulas for the while loop in Line 4. First assert initial state $I$.

\begin{debug}
(assert (DEF__106 0))
(assert (DEF__103 0))
(assert (DEF__105 0))
(assert (DEF__104 0))
\end{debug}

5. then assert the predicates observer \texttt{obs}, so we can observe the predicates in the initial state:

\begin{debug}
(push)
(assert (= (obs 1)(pred_1 0)))
...
(assert (= (obs 8)(pred_7 0)))
(assert (= base 0))
\end{debug}

6. then assert the initial BDD candidate $D_0$, and we check for satisfiability:
\begin{debug}
(push)
(assert (= (node 1 0) true))
(assert (= (node 2 0) false))
(assert (not (node 2 0 )))
(check)
\end{debug}

For this particular set of assertions, we get the following model $s$, c.f. Line 6 from the first algorithm:

\begin{debug}
(= (obs 3) true)
(= (obs 4) false)
(= (obs 1) false)
(= (obs 5) false)
(= (obs 6) false)
(= (obs 8) true)
(= (obs 2) false)
(= (obs 7) false)
\end{debug}

7. We first pop the last set of assertion and assert a new BDD, which is the \texttt{cube} of $s$ joined with the previous BDD, c.f. Line 7:

\begin{debug}
(push)
(assert (= (node 1 0) true))
(assert (= (node 2 0) false))
(assert (= (node 3 0) (ite (pred_8 0) (node 1 0 ) (node 2 0 ))))
(assert (= (node 4 0) (ite (pred_7 0) (node 2 0 ) (node 3 0 ))))
(assert (= (node 5 0) (ite (pred_6 0) (node 2 0 ) (node 4 0 ))))
(assert (= (node 6 0) (ite (pred_5 0) (node 2 0 ) (node 5 0 ))))
(assert (= (node 7 0) (ite (pred_4 0) (node 2 0 ) (node 6 0 ))))
(assert (= (node 8 0) (ite (pred_3 0) (node 7 0 ) (node 2 0 ))))
(assert (= (node 9 0) (ite (pred_2 0) (node 2 0 ) (node 8 0 ))))
(assert (= (node 10 0) (ite (pred_1 0) (node 2 0 ) (node 9 0 ))))
(assert (not (node 10 0 )))
(check)
\end{debug}

8. We check again for satisfiability, and this time we have an UNSAT. At
this point we are out from the while loop in Line 4. We pop the
definitions and we are at level described in line 4 from point 4. We
now assert $T[x,x']$, and the predicate observer at $K=1$:

\begin{debug}
(assert (DEF__106 1))
(assert (DEF__103 1))
(assert (DEF__105 1))
(assert (DEF__104 1))
(assert (= (obs 1)(pred_1 1)))
...
(assert (= (obs 8)(pred_7 1)))
(assert (= base (- 0 1)))
\end{debug}

9. Next we push and assert the BDD candidate $\gamma(D_k)[x]$, c.f. Line 15:

\begin{debug}
(push)
(assert (= (node 1 0) true))
(assert (= (node 2 0) false))
(assert (= (node 3 0) (ite (pred_8 0) (node 1 0 ) (node 2 0 ))))
(assert (= (node 4 0) (ite (pred_7 0) (node 2 0 ) (node 3 0 ))))
(assert (= (node 5 0) (ite (pred_6 0) (node 2 0 ) (node 4 0 ))))
(assert (= (node 6 0) (ite (pred_5 0) (node 2 0 ) (node 5 0 ))))
(assert (= (node 7 0) (ite (pred_4 0) (node 2 0 ) (node 6 0 ))))
(assert (= (node 8 0) (ite (pred_3 0) (node 7 0 ) (node 2 0 ))))
(assert (= (node 9 0) (ite (pred_2 0) (node 2 0 ) (node 8 0 ))))
(assert (= (node 10 0) (ite (pred_1 0) (node 2 0 ) (node 9 0 ))))
\end{debug}

and the BDD candidate $\neg\gamma(D_{k+1})[x']$. In this frist instance $D_k\ = D_{k+1}$.

\begin{debug}
(assert (= (node 1 1) true))
(assert (= (node 2 1) false))
(assert (= (node 3 1) (ite (pred_8 1) (node 1 1 ) (node 2 1 ))))
(assert (= (node 4 1) (ite (pred_7 1) (node 2 1 ) (node 3 1 ))))
(assert (= (node 5 1) (ite (pred_6 1) (node 2 1 ) (node 4 1 ))))
(assert (= (node 6 1) (ite (pred_5 1) (node 2 1 ) (node 5 1 ))))
(assert (= (node 7 1) (ite (pred_4 1) (node 2 1 ) (node 6 1 ))))
(assert (= (node 8 1) (ite (pred_3 1) (node 7 1 ) (node 2 1 ))))
(assert (= (node 9 1) (ite (pred_2 1) (node 2 1 ) (node 8 1 ))))
(assert (= (node 10 1) (ite (pred_1 1) (node 2 1 ) (node 9 1 ))))
(assert (node 10 0))
(assert (not (node 10 1 )))
(check)
\end{debug}

10. If the query is UNSAT, then we are done c.f. Line 18. If the query
is SAT, then we enter into the loop in Line 20-24. For this example we get the following model $s'$:

\begin{debug}
(= (_obs_ 3) false)
(= (_obs_ 4) false)
(= (_obs_ 1) false)
(= (_obs_ 5) true)
(= (_obs_ 6) true)
(= (_obs_ 8) false)
(= (_obs_ 2) false)
(= (_obs_ 7) false)
\end{debug}

11. The first step is to pop the assertion until Line 1 in step 9, and then push $\gamma(D_k)[x]$

\begin{debug}
(pop)
(push)
(assert (= (node 1 0) true))
(assert (= (node 2 0) false))
(assert (= (node 3 0) (ite (pred_8 0) (node 1 0 ) (node 2 0 ))))
(assert (= (node 4 0) (ite (pred_7 0) (node 2 0 ) (node 3 0 ))))
(assert (= (node 5 0) (ite (pred_6 0) (node 2 0 ) (node 4 0 ))))
(assert (= (node 6 0) (ite (pred_5 0) (node 2 0 ) (node 5 0 ))))
(assert (= (node 7 0) (ite (pred_4 0) (node 2 0 ) (node 6 0 ))))
(assert (= (node 8 0) (ite (pred_3 0) (node 7 0 ) (node 2 0 ))))
(assert (= (node 9 0) (ite (pred_2 0) (node 2 0 ) (node 8 0 ))))
(assert (= (node 10 0) (ite (pred_1 0) (node 2 0 ) (node 9 0 ))))
(assert (node 10 0))
\end{debug}

and then a new bdd, which is build by $join(D_{k+1}, cube(s'))$, is asserted:

\begin{debug}
(assert (= (node 1 1) true))
(assert (= (node 2 1) false))
(assert (= (node 18 1) (ite (pred_7 1) (node 2 1 ) (node 3 1 ))))
(assert (= (node 19 1) (ite (pred_6 1) (node 2 1 ) (node 18 1 ))))
(assert (= (node 20 1) (ite (pred_5 1) (node 2 1 ) (node 19 1 ))))
(assert (= (node 21 1) (ite (pred_4 1) (node 2 1 ) (node 20 1 ))))
(assert (= (node 22 1) (ite (pred_8 1) (node 2 1 ) (node 1 1 ))))
(assert (= (node 23 1) (ite (pred_7 1) (node 2 1 ) (node 22 1 ))))
(assert (= (node 24 1) (ite (pred_6 1) (node 23 1 ) (node 2 1 ))))
(assert (= (node 25 1) (ite (pred_5 1) (node 24 1 ) (node 2 1 ))))
(assert (= (node 26 1) (ite (pred_4 1) (node 2 1 ) (node 25 1 ))))
(assert (= (node 27 1) (ite (pred_3 1) (node 21 1 ) (node 26 1 ))))
(assert (= (node 28 1) (ite (pred_2 1) (node 2 1 ) (node 27 1 ))))
(assert (= (node 29 1) (ite (pred_1 1) (node 2 1 ) (node 28 1 ))))
(assert (not (node 29 1 )))
(check)
\end{debug}

For this particular example the asserted formulas (i.e BDD) result is UNSAT. Hence, we are out of the loop in Line 20-23. At this point, we first pop the asserted formulas (and we are back to the level of line 8 of stage 8) and we push a $\gamma(D_k)[x]$:

\begin{debug}
(push)
(assert (= (node 1 0) true))
(assert (= (node 2 0) false))
(assert (= (node 3 0) (ite (pred_8 0) (node 1 0 ) (node 2 0 ))))
(assert (= (node 4 0) (ite (pred_7 0) (node 2 0 ) (node 3 0 ))))
(assert (= (node 5 0) (ite (pred_6 0) (node 2 0 ) (node 4 0 ))))
(assert (= (node 6 0) (ite (pred_5 0) (node 2 0 ) (node 5 0 ))))
(assert (= (node 7 0) (ite (pred_4 0) (node 2 0 ) (node 6 0 ))))
(assert (= (node 8 0) (ite (pred_3 0) (node 7 0 ) (node 2 0 ))))
(assert (= (node 9 0) (ite (pred_2 0) (node 2 0 ) (node 8 0 ))))
(assert (= (node 10 0) (ite (pred_1 0) (node 2 0 ) (node 9 0 ))))
(assert (node 10 0))
\end{debug}

and $\gamma(D_{k+1})[x']$

\begin{debug}
(assert (= (node 1 1) true))
(assert (= (node 2 1) false))
(assert (= (node 30 1) (ite (pred_7 1) (node 2 1 ) (node 22 1 ))))
(assert (= (node 31 1) (ite (pred_6 1) (node 2 1 ) (node 30 1 ))))
(assert (= (node 32 1) (ite (pred_5 1) (node 2 1 ) (node 31 1 ))))
(assert (= (node 33 1) (ite (pred_4 1) (node 2 1 ) (node 32 1 ))))
(assert (= (node 34 1) (ite (pred_8 1) (node 2 1 ) (node 1 1 ))))
(assert (= (node 35 1) (ite (pred_7 1) (node 2 1 ) (node 34 1 ))))
(assert (= (node 36 1) (ite (pred_6 1) (node 35 1 ) (node 2 1 ))))
(assert (= (node 37 1) (ite (pred_5 1) (node 36 1 ) (node 2 1 ))))
(assert (= (node 38 1) (ite (pred_4 1) (node 2 1 ) (node 37 1 ))))
(assert (= (node 39 1) (ite (pred_3 1) (node 33 1 ) (node 38 1 ))))
(assert (= (node 40 1) (ite (pred_2 1) (node 2 1 ) (node 39 1 ))))
(assert (= (node 41 1) (ite (pred_1 1) (node 2 1 ) (node 40 1 ))))
(assert (not (node 41 1 )))
\end{debug}

For this example, at this point the asserted formulas are UNSAT, hence, we have found an invariant (Line 18).
\bibliography{biblio}
\bibliographystyle{alpha}

\end{document}  
