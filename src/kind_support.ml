

(** A module that provides support function for the main KIND module *)

(** 
@author Temesghen Kahsai
*)

open Types
open Flags
open Exceptions
open Channels
open Globals

let solver = Globals.my_solver

let toss x = () (* toss outputs *)


(********************************************************************)
let get_add () = 
    if !do_negative_index then
      -1
    else
      1



(********************************************************************)
let setup_solver_bdd () =
  let solvercall = solver#get#solver_call !Flags.solverflags in
  let bdd_solver, to_ch = Unix.open_process solvercall in
      to_solver_bdd := to_ch;
      bdd_solver


(** Print initial TS*)
 let print_ts solver def_hash k add =
    match solver with
      BDD -> 
          begin
              Hashtbl.iter (fun x y ->
                  match y with
                      DEF_REF _ -> () (* skip *)
                    | DEF_STR d ->
                            begin
                              let s = F_PRED("DEF"^"__"^(string_of_int x),[NUM(add*k)]) in
                                send_to_solver BDD (Lus_convert_yc.simple_command_string (ASSERT(s)));
                            end
                        ) def_hash
          end
        | _ ->
            failwith("Not implemented");
          

