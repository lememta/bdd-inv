
(** First Pass Translation.  This translates from a stream formal into an "IL"-like format. *)

(** A counter to ensure all variable ids are unique. *)
val idcounter : Counter.counter

(** Current line number and position of the last newline.  Used by parser. *)
val linenum : int ref
(** Current line number and position of the last newline.  Used by parser. *)
val linepos : int ref


(** Create a conjunction of two formulas *)
val f_and : Types.il_formula -> Types.il_formula -> Types.il_formula

(** Translate position into an integer *)
val simplify_position_depth : Types.il_expression -> int

(** Convert a {!Types.typed_stream} into a set of {!Types.il_formula}s.  
Any subnode calls are transformed into inlined definitions.  A basic simplification is done to unify obviously redundant variables (of the type a=b). *)
val convert_term : Types.typed_stream -> Types.il_expression -> int -> int ->  (string * Types.transtable) option -> Types.typed_stream option -> (Types.il_expression * Types.il_formula)

(** Returns the maximun depth of pres in the program *)
val get_max_depth : unit -> int

(** Returns the maximum depth of followdbys in the program *)
val get_max_adepth : unit -> int


(** Returns a list of stateful variables (for use when we refine them as a preprocessing step) *)
val state_vars_list : unit -> Types.idtype list

val convert_def_list : Types.il_expression -> Types.node_id_t -> Types.il_formula

val convert_equation : Types.typed_stream ->  Types.il_expression -> Types.il_formula


