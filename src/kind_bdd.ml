(** Kind-BDD. Main entry point. *)

(**
@author Temesghen Kahsai

*)

open Types
open Exceptions
open Channels
open Globals


let solver = Globals.my_solver

(********************************************************************)
let print_copyright () =
  print_string ( "TBD" )

(********************************************************************)
let start fn =
    begin
      match !Flags.set_my_solver with
        YICES ->
          failwith("YICES EXEC is not supported yet for generation of invariants.")
      | YICES_WRAPPER ->
	       if(Solvers_path.yicesw_path = "no") then
	       (
		      solver#set (new Solver_yices_cmdline.solver_yices_cmdline)
	       )else(
		      solver#set (new Solver_yices.solver_yices)
	        )
      | _ ->
          failwith("Kind-BDD currently works only with YICES")
    end;
    if !Flags.do_scratch then (bdd_scratch := open_out (fn^"."^solver#get#file_extension^"_bdd"));
    if !Flags.bddGraph then (bdd_dot := open_out (fn^".dot"));

    Bdd_loop.make_invariant fn;;
    (* if !Flags.bddGraph then (
        let _ = close_out !bdd_dot in
        let dot = fn^".dot" in
        let pdf = dot^".pdf" in
        let cmd = "neato -Tpdf " ^ dot ^ " -o " ^ pdf ^ "; open " ^ pdf in
        Sys.command cmd; ()
    )   *)





(********************************************************************)
let _ =
  let usage_msg = "Generate invariants using BDD for single node Lustre program.\nUsage: kind_bdd [options] input_file" in
  let speclist = [
                   ("-node", Arg.String (fun x -> Flags.user_specified_main_node_name := x),
		                  "\tCONTROL: User specified main node (default: the last node that contains a PROPERTY)");

		                ("-scratch",
                    Arg.Unit (fun () -> Flags.do_scratch := true),
                    "\tOUTPUT: Produce files for debugging purposes. This option will produce 3 files (*_bdd). (default: Don't log to disk)"
                   );

                   ("-v",
                    Arg.Unit (fun () -> Flags.loud := true;
                                        Flags.final_cex_loud := true),
                    "\tOUTPUT: Return more details (default: false)"
                   );

                   ("-g",
                    Arg.Unit (fun () -> Flags.bddGraph := true),
                    "\tOUTPUT: Produce .dot of the BDD graph for every iteration. (default: false)"
                   );

                  ("-c",
                    Arg.Unit (fun () -> Flags.coi := true),
                    "\tOUTPUT: Do Cone Of Influence (default: false)"
                   );


                   ("-f",
                    Arg.Unit (fun () -> Flags.forceGraph := true),
                    "\tOUTPUT: Produce .dot of the BDD graph for every iteration. (default: false)"
                   );

                   ("-n",
                    Arg.Int (fun x -> Flags.num_inv := x),
                    "\tOUTPUT: Set the number of candidates (default: all)"
                   );


                  ("-guard",
                    Arg.Unit (fun () -> Flags.guard := true),
                    "\tOUTPUT: Predicates based on boolean predicates from the program (default: false)"
                    );

                   ("-version",
                      Arg.Unit (fun () -> print_copyright()),
                      "Print version & copyright information"
                   )
                 ]
  in
  Arg.parse speclist start usage_msg
