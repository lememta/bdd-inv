
(** Global declartion of channnels *)

(** 
@author Temesghen Kahsai

*)

open Types

(* channels used, and basic printings *)

let to_solver_bdd = ref stdout (* sent to bdd solver *)
let dummy = stdin (* dummy *)
let debug_ch = ref stdout  (* what the user sees *)
let xml_ch = ref stdout (* for generation of XML document *)

let bdd_scratch = ref stdout (* For the bdd based invariant generation *)
let bdd_dot = ref stdout (* Dot file for bdd *)

let debug_to_user st = () 

let send_to_solver solver str =
  match solver with
   BDD -> 
   begin
    if !Flags.do_scratch then output_string !bdd_scratch str;
      output_string !to_solver_bdd str;
    if !Flags.loud then output_string !debug_ch str;flush_all ()
  end

    | _ -> assert false


      
(* Debug each processes *)
let debug_proc proc verbose str = 
 
  if !Flags.loud then
    (
    match proc, verbose with 
	 BDD_PROC, Some true->
    begin
      let s = "BDD : "^str^"\n" in
        output_string !debug_ch s;
        flush_all ()
    end
      | _ , None -> flush_all ()
    ); 
  if !Flags.do_scratch then
    (
    match proc with 
	 BDD_PROC ->
    begin
      let s = "BDD : "^str^"\n" in
        output_string !bdd_scratch s;
        flush_all ()
    end
      | _ -> flush_all ()
    )	  


(* Print XML document*)
let print_xml str =
  output_string !xml_ch str;
  flush_all ()
