open Printf;;

let main f =
 let mapName = f^".map" in
  let cmd = sprintf "dot -Tcmapx -o%s -Tgif -o%s.gif %s.dot" mapName f f in
    printf "Execute '%s'\n" cmd;
    ignore(Sys.command cmd);
    let ch = open_out (sprintf "%s.html" f) in
      fprintf ch "<html>\n";
      fprintf ch "<img src='%s.gif' usemap=%S/>" f f;
      (* Append f.map to f.html *)
      let ich = open_in mapName in
        try
            while true do
                fprintf ch "%s\n" (input_line ich);
            done
        with End_of_file -> close_in ich;
        fprintf ch "</html>\n";
        close_out ch;
        (* remove f.map *)
        Sys.remove mapName
;;


let _ =
    try
        let f =
            try Str.replace_first (Str.regexp "\\.dot") "" Sys.argv.(1) 
            with Not_found -> Sys.argv.(1)
      in main f
    with Invalid_argument(_) -> printf "Usage: dot2html <file>\n"; exit 1