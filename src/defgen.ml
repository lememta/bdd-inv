
(* Definition Generation *)

open Types
open Exceptions
open Channels
open Printf



let solver = Globals.my_solver


(********************************************************************)
(* returns string buffer *)
(* ite_maxdepth should be > 0 for ite elim mode 'n' only *)
(* ite_maxdepth should be < 0 for ite elim mode 'i' only *)
let generate_var_defs_yc defhash def_base_name ite_maxdepth =
  let buf = Buffer.create 100 in
  Hashtbl.iter (fun x y ->
    match y with
        DEF_REF _ -> ()
      | DEF_STR dhe ->
          if ite_maxdepth >= 0 then
              solver#get#define_fun_buffer buf 
               (def_base_name^"__"^(string_of_int x)) (M_FUNC[M_NAT;M_BOOL])
               [(solver#get#position_var1,M_NAT)] dhe.def
          else
              solver#get#define_fun_buffer buf 
               (def_base_name^"__"^(string_of_int x)) M_BOOL [] dhe.def
    
  ) defhash;
  buf

    
(* (\** Generate formulas.*\) *)
(*  let start filename =    *)
(*   let use_file = open_in filename in *)
(*   let in_ch = use_file in *)
(*   let lexbuf = Lexing.from_channel in_ch in *)
(*   let outdoc = Buffer.create 1000 in *)
(*   let def_hash = Deftable.get_defhash() in *)
(*   let no_stateful = ref false in *)

(*   try *)
(*     let (cp, p, list_p, target_node) = Lustre_parser.main Lustre_lexer.token lexbuf in *)
(*     let position = POSITION_VAR (solver#get#position_var1) in *)
(*     let _ = if (!Flags.abstr_bool || !Flags.abstr_ite || !Flags.abstr_pre) then  *)
(*       Lus_flatten.flatten_all_node_defs () in *)

(*     let fd = Lus_convert.convert_def_list position target_node in *)

(*     let maxdepth = Lus_convert.get_max_depth() in *)
(*     if maxdepth > Lus_convert.get_max_adepth() then raise Unguarded_PRE; *)
  
(*     let fd' = *)
(*       if !Flags.aggressive_inlining > 0 then *)
(*         Inlining.inlined_formula fd (Coi.property_id_list p)  *)
(*       else *)
(*         fd *)
(*     in *)
   
(*     (\* verfs lists the initial set of variables -- properties, basically *\) *)
(*     let _ =  Coi.examine_assignments fd' in *)
(*     let vrefs1 = Coi.property_id_list p in *)
(*     let vrefs =  *)
(*       if !Flags.pre_refine_state_vars then *)
(*         List.rev_append vrefs1 (Lus_convert.state_vars_list()) *)
(*       else *)
(*         vrefs1 *)
(*     in *)
(*       Coi.calculate_all_dependencies vrefs 0 maxdepth; *)
     
(*     begin  *)
(*       let form_str_buf = Lus_convert_yc.yc_formula_string_buffer GENERAL maxdepth fd' in *)
(*       let property =  Lus_convert_yc.yc_property_header solver#get#position_var1 position p in *)
(*       let assertions = Lus_convert_yc.yc_assumption_string solver#get#position_var1 position in *)
(*       let vdef = Lus_convert_yc.yc_var_shortcut_string () in *)
(*         Buffer.add_string outdoc (solver#get#header_string^"\n"); *)
(*         Buffer.add_string outdoc (vdef^"\n\n"); *)

(*         if (!Flags.var_defs_mode) then ( *)
(*             Deftable.initialize_defs def_hash vrefs;  *)
(*             Buffer.add_buffer outdoc (generate_var_defs_yc def_hash "DEF" 0) *)
(*         )else( *)
(*             solver#get#aggdef_header outdoc form_str_buf; *)
(*         ); *)

(*         if !Flags.no_multi then ( *)
(*              Buffer.add_string outdoc (property^"\n"); *)
(*          ) else if (List.length list_p == 1) then ( *)
(*              Buffer.add_string outdoc (property^"\n"); *)
(*          ); *)
        
(*         Buffer.add_string outdoc (assertions^"\n"); *)
(*         if (!Flags.loopfree_mode || !Flags.termination_check) then *)
(*           begin *)
(*             Coi.clean_used_vars maxdepth; *)
(*             try *)
(*               Buffer.add_string outdoc  *)
(*                 ((Lus_convert_horn.state_vars_string())^"\n"); *)
(*             with NoStatefulVars -> *)
(*               no_stateful := true; *)
(*           end; *)
(*       end; (\* end yices formula generation *\) *)
(*     (\* return values *\) *)
(*       (Buffer.contents outdoc), *)
(*     maxdepth, *)
(*     def_hash, *)
(*     !no_stateful, *)
(*     vrefs, *)
(*     list_p *)
(*   with TypeMismatch(s,x,y) -> *)
(*     Printf.printf "\nType Mismatch %s:\n%s\n!=\n%s\n at line %d (col %d)\n" s *)
(*       (solver#get#type_string x) (solver#get#type_string y)  *)
(*       (!Lus_convert.linenum)  *)
(*       ((Lexing.lexeme_start lexbuf) - !Lus_convert.linepos); *)
(*     raise Lus_convert_error *)
(*     | Parsing.Parse_error -> *)
(*   Printf.printf "\nParse error at line %d (col %d): '%s'\n"  *)
(*     (!Lus_convert.linenum)  *)
(*     ((Lexing.lexeme_start lexbuf) - !Lus_convert.linepos) *)
(*     (Lexing.lexeme lexbuf); *)
(*   raise Lus_convert_error *)


(********************************************************************)
 let start_bdd filename =
  let use_file = open_in filename in
  let in_ch = use_file in
  let lexbuf = Lexing.from_channel in_ch in
  let outdoc = Buffer.create 1000 in
  let def_hash = Deftable.get_defhash() in
  let no_stateful = ref false in

  try
    let (cp,p,list_p, target_node) = Lustre_parser.main Lustre_lexer.token lexbuf in
    let position = POSITION_VAR (solver#get#position_var1) in

    let fd = Lus_convert.convert_def_list position target_node in
    let maxdepth = Lus_convert.get_max_depth() in
      if maxdepth > Lus_convert.get_max_adepth() then raise Unguarded_PRE;

    (* verfs lists the initial set of variables -- properties, basically *)
    let _ = Coi.examine_assignments fd in
    let vrefs = Coi.property_id_list p in
    Coi.calculate_all_dependencies vrefs 0 maxdepth;
    (* if !Flags.coi then ( *)
    (*   Coi.calculate_all_dependencies vrefs 0 maxdepth; *)
    (* ); *)
    begin
        let form_str_buf = Lus_convert_yc.yc_formula_string_buffer GENERAL maxdepth fd in
        let assertions = Lus_convert_yc.yc_assumption_string solver#get#position_var1 position in
        let vdef = Lus_convert_yc.yc_var_shortcut_string () in
        (*let bdd_solver_specific = "\n(define-type _base_t (subtype (n::int) (<= n 0)))\n"
                                  ^"(define _base::_base_t)\n"
                                  ^"(define-type _nat (subtype (n::int) (>= n _base)))\n"
                                  ^"(define _n::_nat)\n"
                                  ^"(define _check_quant::bool)\n"
                                  ^"(set-evidence! true)" in*)
        let bdd_solver_specific = "\n(define _base::int)\n"
                                  ^"(define-type _nat int)\n"
                                  ^"(define _n:: int)\n"
                                  ^"(define _check_quant::bool)\n"
                                  ^"(set-evidence! true)\n" in
          Buffer.add_string outdoc (solver#get#cc^" ----  ^^^^ ------");
          Buffer.add_string outdoc (bdd_solver_specific);
          Buffer.add_string outdoc (vdef^"\n");
          Deftable.initialize_defs def_hash vrefs; 
          Buffer.add_buffer outdoc (generate_var_defs_yc def_hash "DEF" 0);
          Buffer.add_string outdoc (assertions^"\n");
          Buffer.add_string outdoc (solver#get#cc^" ----  ^^^^ ------\n")
    end; 
    (Buffer.contents outdoc), maxdepth, def_hash, !no_stateful, vrefs, list_p
  with TypeMismatch(s,x,y) ->
    Printf.printf "\nType Mismatch %s:\n%s\n!=\n%s\n at line %d (col %d)\n" s
      (solver#get#type_string x) (solver#get#type_string y) 
      (!Lus_convert.linenum) 
      ((Lexing.lexeme_start lexbuf) - !Lus_convert.linepos);
    raise Lus_convert_error
  | Parsing.Parse_error ->
    Printf.printf "\nParse error at line %d (col %d): '%s'\n" 
      (!Lus_convert.linenum) 
      ((Lexing.lexeme_start lexbuf) - !Lus_convert.linepos)
      (Lexing.lexeme lexbuf);
    raise Lus_convert_error







