 open Types
 open Flags
 open Channels
 open Exceptions
 open Types
 open Globals
 open Expr_util

 let toss x = () (* toss outputs *)
 let solver = Globals.my_solver
 exception InvalidStep of string


 module Macros = Bdd_helper.Macros

 module Predicates =
   struct
     (** Generate predicates from mode variables*)
     let gn_predicate_mode () =
       let _ = debug_proc BDD_PROC (Some true) "Generating mode variable predicates" in
       let predicates =
         List.map (fun (v,lb,ub) ->
                   let mode_vars, mode_ls = New_vars.mk_pos_candidate v lb ub  in
                   List.map(fun (typ,def) ->
                            Bdd_helper.add_predicate typ def);
                   mode_vars
                 )(New_vars.get_interval_vars()) in
         predicates

     (** Generate predicates from boolean predicates *)
     let gn_predicate_bool () =
       let _ = debug_proc BDD_PROC (Some true) "Generating ITE guards predicates" in
       let all_terms = Lus_convert_yc.get_all_def_terms () in
       let bool_sub_exprs, int_sub_exprs, float_sub_exprs = Sub_exprs.sub_exprs all_terms Expr_util.filter_sub_exprs in
        New_vars.get_bool_nvrs (), New_vars.ite_newvar_defs bool_sub_exprs

     (*Assert a predicate at a position k*)
     let assert_predicate k = New_vars.mk_nvr_eq_cmds (NUM k)

     let assertObs obs_ids k =
             List.fold_right(fun x acc ->
               let pred = New_vars.get_candidate x in
               let assert_pred = "(assert (= (_obs_ " ^ string_of_int(x)
                                 ^ ")(" ^pred ^ " "^string_of_int(k)^")))\n" in
                         assert_pred^acc
           ) obs_ids ""

     let assertPred obs_ids k =
             List.fold_right(fun x acc ->
               let pred = New_vars.get_candidate x in
               let assert_pred = "(assert (" ^pred ^ " "^string_of_int(k)^"))\n" in
                         assert_pred^acc
           ) obs_ids ""

     let debugNewCand newCandExpr =
       debug_proc BDD_PROC (Some true) "----------NEW CANDIDATES-----------";
       List.iter(fun (x,y,z) ->
         debug_proc BDD_PROC (Some true) (x^" -> " ^ y ^ " || " ^ z))newCandExpr;
       debug_proc BDD_PROC (None) "-----------------------------------\n";
   end

 let bdd = new Bdd_helper.bddBank

 module Bdd_invariants =
   struct
     let graph = new Bdd_graph.bddGraph

     (*Base case*)
     let base_case sp k preds pred_ids = ()

     let smt_checker sp assertions =
       if (solver#get#result_is_unsat assertions) then(
           debug_proc BDD_PROC (Some true) "UNSAT";
           UNSAT)
       else if(solver#get#result_is_sat assertions) then(
         debug_proc BDD_PROC (Some true) "SAT";
         SAT)
       else if (solver#get#result_is_error assertions) then(
         debug_proc BDD_PROC (Some true) "ERROR";
         debug_proc BDD_PROC (Some true) assertions;
         ERROR)
       else if (solver#get#result_is_unknown assertions) then(
         debug_proc BDD_PROC (Some true) "UNK";
         debug_proc BDD_PROC (Some true) assertions;
         ERROR
       )
     else(
       assert false
     )

   let query = "(check)\n"

   (* Recursive call to get the invariant at the initial states -- Lines 2-11*)
   let pre_processing sp preds pred_ids =
     let cnt = new Counter.counter in
     let _ = send_to_solver BDD solver#get#push_string in
     let _ = Kind_support.print_ts BDD sp.def_hash 0 sp.add in
       if Lus_assertions.assertions_present() then (
         let ass =  Lus_convert_yc.simple_command_string(ASSERT(F_PRED(solver#get#assertions,[NUM(sp.add*0)]))) in
         send_to_solver BDD (ass^"\n")
       );
     let pred_k = Predicates.assertObs pred_ids 0 in
     let bdd_f = bdd#get_current_bdd() in
     let init_rootNode, init_lstExpr = bdd#initGamma bdd_f in
     (* bdd#print_nhtbl("106");  *)
     let initCandExpr = List.rev init_lstExpr in
     let cand, _ = bdd#mkNewCandidate initCandExpr 0 in
     let initCandidate = Buffer.contents (cand) in
     let root = Macros.rootQuery 1 0 in
     let _ = send_to_solver BDD (pred_k
                                 ^"(assert (= _base 0))\n"
                                 ^solver#get#push_string
                                 ^initCandidate
                                 ^root
                                 ^query
                                 ^solver#get#done_string) in
     let out = solver#get#get_solver_output BDD_PROC sp.bdd_solver in
       match smt_checker sp out with
         |UNSAT -> failwith ("FAILED AT INITIAL CASE WITH BDD = FALSE")
         |SAT ->
           begin
            let simulation = solver#get#get_simulation_cex BDD_PROC out sp.bdd_solver in
            let _ = send_to_solver BDD solver#get#pop_string in
            let cubedBdd0 = bdd#cube simulation in
            let bddFalse = bdd#get_current_bdd () in
            let bddK0 = bdd#join bddFalse cubedBdd0 in
            if !Flags.loud then (
             bdd#print_bdd cubedBdd0 "CubedBDD: ";
             bdd#print_bdd bddFalse "Previous BDD: ";
             bdd#print_bdd bddK0 "Joined BDD: ";
             );
            bdd#set_current_bdd bddK0;
            let rootNode, lstExpr = bdd#gamma bddK0 0 in
            bdd#set_current_root rootNode;
            let newCandExpr = List.rev lstExpr in
            if !Flags.bddGraph then (
               let bddStore = bdd#get_bddStore () in
               let i, g = graph#gammaDot bddK0 bddStore in
                 graph#mkDot g ("pre_"^(string_of_int cnt#next)));
             if !Flags.forceGraph then (graph#printDot());
             let newCandidates,_ = bdd#mkNewCandidate newCandExpr 0 in
             newCandidates, rootNode
           end
         |ERROR ->
           failwith("SMT SOLVER : Error in pre_processing")

     (* Lines 2 -11 *)
     let rec initStateInvariants sp gamma_D0 rootNode k =
       debug_proc BDD_PROC (Some true) "Iterating in the initial case (Lines 2-11)";
       let cnt = new Counter.counter in
       let pred_k = Macros.rootQuery rootNode k in
       let _ = send_to_solver BDD (solver#get#push_string
                                   ^(Buffer.contents gamma_D0)
                                   ^pred_k
                                   ^query
                                   ^solver#get#done_string) in
       let out = solver#get#get_solver_output BDD_PROC sp.bdd_solver in
       match smt_checker sp out with
         |UNSAT ->
           begin
             debug_proc BDD_PROC (Some true) "Finished Initial State Invariants (Line 11)";
             let _ = send_to_solver BDD solver#get#pop_string in
             let _ = send_to_solver BDD solver#get#pop_string in
             raise InitialStateInv;
           end
         |SAT ->
           begin
            let simulation = solver#get#get_simulation_cex BDD_PROC out sp.bdd_solver in
            let _ = send_to_solver BDD solver#get#pop_string in
            let cubedBdd = bdd#cube simulation in
            let current_bdd = bdd#get_current_bdd () in
            let new_bdd = bdd#join current_bdd cubedBdd in
            if !Flags.loud then (
             bdd#print_bdd cubedBdd "CubedBDD: ";
             bdd#print_bdd current_bdd "Previous BDD: ";
             bdd#print_bdd new_bdd "Joined BDD: ";
             );
            let _ = bdd#set_current_bdd new_bdd in
            let newRootNode, lstExpr = bdd#gamma new_bdd k in
            bdd#set_current_root newRootNode;
            let newCandExpr = List.rev lstExpr in
            let newCandidates,_ = bdd#mkNewCandidate newCandExpr k in
            initStateInvariants sp newCandidates newRootNode k
           end
         |ERROR ->
           failwith("SMT SOLVER : Error in initStateInvariants")

     let iteration_to_approx = ref 0
     let it = ref 0

     let rec kFixedIter sp cex bddK1 bddK0_g rootK0 bddNum_s =
       (*
           bddK1 :: the next bdd to be modified
           bddK0_g :: static bdd
           rootK0 :: static root Bdd
       *)
       let k = 1 in
       debug_proc BDD_PROC (Some true) ("Fixed Point iteration for K: " ^ bddNum_s ^ " Line 18-24");
       let cubedBdd = bdd#cube cex in
       let newBdd = bdd#join bddK1 cubedBdd in (* Line 22 *)
       if !Flags.loud then (
         bdd#print_bdd bddK1 ("OLD_BDD_" ^ string_of_int(!it));
         bdd#print_bdd cubedBdd ("CUBED_BDD_" ^ string_of_int(!it));
         bdd#print_bdd newBdd ("JOINED_BDD_" ^ string_of_int(!it))
       );
       let _ = bdd#set_stable_bdd newBdd in
       let newRootNode, lstExpr = bdd#gamma newBdd (k-1) in
       let newCandExpr = List.rev lstExpr in
       let currentBuff, nextBuff = bdd#mkNewCandidate newCandExpr (k-1) in
       let rootK = Macros.rootQuery newRootNode k in
       let _ = send_to_solver BDD (solver#get#push_string
                                   ^ bddK0_g
                                   ^(Buffer.contents nextBuff)
                                   ^rootK0
                                   ^rootK
                                   ^query
                                   ^solver#get#done_string) in
       let out = solver#get#get_solver_output BDD_PROC sp.bdd_solver in
       match smt_checker sp out with
         |UNSAT ->
           begin
                let _ = send_to_solver BDD solver#get#pop_string in
                   newBdd
            end
         |SAT ->
           begin
            let simulation = solver#get#get_simulation_cex BDD_PROC out sp.bdd_solver in
            let _ = send_to_solver BDD solver#get#pop_string in
            it := !it + 1;
            let it_s = string_of_int !it in
            if !Flags.bddGraph then (
                 let bddStore = bdd#get_bddStore () in
                 let i, g = graph#gammaDot newBdd bddStore in
                 graph#mkDot g ("fixedIter_"^it_s)
             );
           kFixedIter sp simulation newBdd bddK0_g rootK0 bddNum_s
           end
         |ERROR ->
           failwith("Error in pre_processing")

     let first_pass = ref true
     let dkx = ref ""
     let stable = ref true
     let bddNum = ref 1
     let previousRootNode = ref 0

     (** Iterating to find invariants iterating over K BDDs *)
     let rec kStateInvariants sp kBdd =
       let _ = bdd#set_next_bdd kBdd in (* D_k+1 bdd *)
       let k = 1 in
       let k_s =  (string_of_int k) in
       let bddNum_s = (string_of_int !bddNum) in
       debug_proc BDD_PROC (Some true) ("Calculating state invariants with BDD Number : " ^ (bddNum_s) ^ " (Line 12-24)");
       let _ = if !stable then dkx := bdd#get_current_gBDD () in
       let bddK0_g = bdd#get_current_gBDD () in
       let bddTBC, rootBDD =
               if !first_pass then(
                   debug_proc BDD_PROC (Some true) "Initial pass in state invariants";
                   let bddK0 = bddK0_g ^ "\n" ^ (bdd#get_next_gBDD ()) in
                   let currentRoot = bdd#get_current_root () in
                   previousRootNode := currentRoot;
                   bddK0 , currentRoot
               )else(
                   let newRootNode, lstExpr = bdd#gamma kBdd (k-1) in
                   let newCandExpr = List.rev lstExpr in
                   let newCand_0, newCand_1 = bdd#mkNewCandidate newCandExpr (k-1) in
                       !dkx^(Buffer.contents newCand_1), newRootNode
                   ) in

       let rootSMT = Macros.rootQueryAssert !previousRootNode (k-1) in
       let rootSMT_TBC = Macros.rootQuery rootBDD (k) in
       let _ = send_to_solver BDD (solver#get#push_string
                                   ^bddTBC
                                   ^rootSMT
                                   ^rootSMT_TBC
                                   ^query
                                   ^solver#get#done_string) in
       let out = solver#get#get_solver_output BDD_PROC sp.bdd_solver in
       match smt_checker sp out with
         |UNSAT ->
           begin
             let _ = send_to_solver BDD solver#get#pop_string in
             if !Flags.loud then (
               bdd#print_bdd kBdd ("FINAL BDD: ");

             );
             let finalInv = bdd#ppInv kBdd in
             print_string finalInv;
           end
         |SAT ->
           begin
            let simulation = solver#get#get_simulation_cex BDD_PROC out sp.bdd_solver in
            let _ = send_to_solver BDD solver#get#pop_string in
            (* Find BDD fixed point iteration *)
            let bddK1 = bdd#get_next_bdd () in
            let k_bdd = kFixedIter sp simulation bddK1 bddK0_g rootSMT bddNum_s in
             if !Flags.bddGraph then (
                 let bddStore = bdd#get_bddStore () in
                 let i, g = graph#gammaDot k_bdd bddStore in
                 graph#mkDot g ("step_"^k_s)
             );
            stable := false;
            if !Flags.loud then (
               bdd#print_bdd k_bdd ("BDD AT " ^ bddNum_s)
             );
            first_pass := false;
            bddNum := !bddNum + 1;
            kStateInvariants sp k_bdd;
           end
         |ERROR ->
           failwith("Error in pre_processing")



     let gn_defs filename =
       let defdoc, maxdepth, def_hash, _, _, _ = Defgen.start_bdd filename in
       let add = Kind_support.get_add() in
       let nstart = solver#get#step_base_string in
       let nvar = solver#get#k_position_string in
       let startdepth = maxdepth + 1 in
       let bdd_solver = Kind_support.setup_solver_bdd () in
         send_to_solver BDD (defdoc^"\n"); (* Print TS definitions *)
        {
         add = add;
         startdepth = startdepth;
         nstart = nstart;
         bdd_solver = bdd_solver;
         cur_depth = maxdepth;
         nvar = nvar;
         def_hash = def_hash;
       }

     let init filename =
         let _ = debug_proc BDD_PROC (Some true) (" BDD Invariant for "^filename) in
         let p = gn_defs filename in

         (*Various check*)
         if ((not !Globals.is_inter) && (not !Flags.guard) && (!Flags.num_inv == 0)) then failwith("No mode variables")
                 else debug_proc BDD_PROC (Some true) ("Mode variables? OK");
         if (!Flags.num_inv < 0) then failwith("Lets be serious, negative number, seriously?");
         if ((not !Flags.guard) && (!Flags.num_inv > 0)) then (Flags.guard := true);
         (* end *)

         let (bool_pred, bool_pred_buf) = if !Flags.guard then Predicates.gn_predicate_bool () else ([],"") in (* generate predicates *)
         let predicates = if !Flags.guard then [] else Predicates.gn_predicate_mode () in
         let obs = Macros.mkObs "_obs_" in
         let bddNode = Macros.mkNode "_node_" in
         let _ = if !Flags.guard then (send_to_solver BDD bool_pred_buf) else List.iter(fun x -> send_to_solver BDD x) predicates in
         let _ = send_to_solver BDD (obs^bddNode) in
         let pred_ids = New_vars.get_ids () in
         let pred_names = New_vars.get_pred_names () in
         let _ = bdd#mk_bdd_int pred_ids pred_names in
         p, predicates, pred_ids;
     end;;


 (** Main entry point *)
 let make_invariant fn =
   (*Get list of predicates and their ids s*)
   let sol, preds, pred_ids = Bdd_invariants.init fn in
   let gamma_D0, initRootNode = Bdd_invariants.pre_processing sol preds pred_ids in
   try
     Bdd_invariants.initStateInvariants sol gamma_D0 initRootNode 0
   with InitialStateInv ->
        debug_proc BDD_PROC (Some true) ("=======================================");
        let kbdd = bdd#get_current_bdd () in
        if !Flags.loud then (
          bdd#print_bdd kbdd "INIT STATE BDD";
        );

        (* Assert T[x,x'] and the observation of predicates at x'*)
        let obs_k = Predicates.assertObs pred_ids 1 in
        Kind_support.print_ts BDD sol.def_hash 1 sol.add;
        if Lus_assertions.assertions_present() then (
          let ass =  Lus_convert_yc.simple_command_string(ASSERT(F_PRED(solver#get#assertions,[NUM(sol.add*1)]))) in
          send_to_solver BDD (ass^"\n")
        );
        send_to_solver BDD (obs_k^("(assert (= _base (- 0 1)))\n"));
        Bdd_invariants.kStateInvariants sol kbdd;
