
(** translate {!Flags.do_negative_index} to a [1] or [-1] to acount for positive or negative indices *)
val get_add : unit -> Types.addtype 

(** Open channels to the solver for BDD*)
val setup_solver_bdd : unit -> in_channel 

(** Print out definitions for initial TS *)
val print_ts : Types.solver_type -> Types.defhashtable -> int -> Types.addtype -> unit



