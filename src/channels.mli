

(** Global declaration of input/ouput channnels and basi printing*)

(** 
@author Temesghen Kahsai

*)

(** Dot file for bdd *)
val bdd_dot : out_channel ref

val bdd_scratch : out_channel ref



(** Print to bdd solver *)
val to_solver_bdd : out_channel ref

(** A dummy channel *)
val dummy : in_channel 

(** which channel to use for debug info (usually main_ch) *)
val debug_ch : out_channel ref


(** A general send string to solver *)
val send_to_solver : Types.solver_type -> string -> unit



(** send a string to the user (also copied to solver 1), if in "debug" mode({!Flags.debug}=[true]). Prepends a comment character and appends a newline. *)
val debug_to_user : string -> unit

(**  Debug information of the various processes *)
val debug_proc : Types.proc -> bool option -> string  -> unit


