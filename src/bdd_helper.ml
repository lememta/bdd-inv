 (** BDD Helper module *)

 (**
 @author Temesghen Kahsai
 *)
 open Channels
 open Types
 open Exceptions
 open Cudd


 let predicate_table = (Hashtbl.create 100:(string,string*string)Hashtbl.t)

 let cnt = new Counter.counter

 let mk_predicate_key s = s^"__"^(string_of_int cnt#next)

 let add_predicate typ def =
   let key = (mk_predicate_key "prd") in
   Hashtbl.replace predicate_table key (typ, def)

 let print_htbl proc htbl =
   Hashtbl.iter(fun x (y,z)->
                let key_value = x^" -> ("^y^" , "^z^")" in
                debug_proc proc (Some true) key_value) htbl


 module Macros =
   struct
     let mkObs obs = "(define "^obs^" ::(-> _nat bool))\n"
     let mkNode node = "(define "^node^" ::(-> _nat _nat bool))\n"
     let rootQueryAssert rootNode k =
       let k_str  = string_of_int k in
       "(assert (_node_ "^ (string_of_int rootNode) ^" "^ k_str ^ " ))\n"
     let rootQuery rootNode k =
       let k_str  = string_of_int k in
       "(assert (not (_node_ "^ (string_of_int rootNode)^" "^ k_str ^ " )))\n"
   end

 class ['a] bddBank =
   let man = Man.make_v ~numVars:10 () in

   let cnt = new Counter.counter in

   let bddCnt = new Counter.counter in
   object(self)
     val mutable current_bdd = Bdd.dfalse man

     val mutable next_bdd = Bdd.dfalse man

     val mutable stable_bdd = Bdd.dfalse man

     val mutable bdd_store = (Hashtbl.create 100:(int, string * Cudd.Man.v Cudd.Bdd.t)Hashtbl.t)

     val mutable gammaBDD = (Hashtbl.create 100:(string, string)Hashtbl.t)

     (* val mutable nodeHtbl = (Hashtbl.create 100:(Cudd.Man.v Cudd.Bdd.t, string)Hashtbl.t) *)

     val mutable nodeHtbl = (Hashtbl.create 100:(Cudd.Man.v Cudd.Bdd.t, int)Hashtbl.t)

     val mutable equalNode = (Hashtbl.create 100:(string, string*(string*string*string)list)Hashtbl.t)

     val mutable rootNode = 0

     val mutable areEqual = false

     method get_bddStore () =  bdd_store

     method safe_find ht x s =
       try
         Hashtbl.find ht x
       with  Not_found ->
         raise (IdNotFound s)

     (** Create a bdd for each obs of predicate *)
     method mk_bdd_int obs_int obs_names =
       List.map2(fun x y  ->
                 let bdd_var = Bdd.ithvar man x in
                 Hashtbl.add bdd_store x (y, bdd_var)
                ) obs_int obs_names

     method private is_true str =
       let up_value = Char.uppercase (String.get str 0)  in
       match up_value with
       | 'T' -> true
       | '_' -> true (*To eliminate _base*)
       | _ -> false

     method private is_false str = not (self#is_true str)

     method private mk_And bdd_list =
       let rec conj bl=
         match bl with
         | [] -> Bdd.dtrue man
         | [(_,a)] -> Bdd.dand a (Bdd.dtrue man)
         | [(_,a);(_,b)] -> Bdd.dand a b
         | (_,h)::tl -> Bdd.dand h (conj tl) in
       conj bdd_list

     method private fold_table_item x y list = (x,y)::list

     method private fold_hash_table table = Hashtbl.fold self#fold_table_item table []

     method cube cex =
       let cex_tbl = self#fold_hash_table cex in
       let t_tmp, f_tmp = List.partition(fun (x,y) -> self#is_true y) cex_tbl in
       let t_ids = List.map(fun (x,y) -> (int_of_string x)) t_tmp in
                         let f_ids = List.map(fun (x,y) -> (int_of_string x)) f_tmp in
                         let t_bdd = List.map(fun x -> (self#safe_find bdd_store x "Cube")) t_ids in
                         let f_bdd = List.map(fun x -> (self#safe_find bdd_store x "Cube")) f_ids in
                         let f_bdd_not = List.map(fun (x,y) -> (x,Bdd.dnot y)) f_bdd in
                         let t_and = self#mk_And t_bdd in
                         let f_and = self#mk_And f_bdd_not in
                         let t_f_and = Bdd.dand (t_and) (f_and) in
                         t_f_and

     method join (bdd_1 : Cudd.Man.v Cudd.Bdd.t) (bdd_2 : Cudd.Man.v Cudd.Bdd.t) =
       let joined_bdd = Bdd.dor bdd_1 bdd_2 in
       joined_bdd

     method overapprox (bddF : Cudd.Man.v Cudd.Bdd.t) =
       let nvars = Bdd.supportsize  bddF in
       debug_proc BDD_PROC (Some true) ("nvars: " ^ (string_of_int nvars));
       Bdd.overapprox nvars 1 true 1.0 bddF

     method private nodeName id = string_of_int id

     method private mkNameK id k =
       let c_k = string_of_int k in
       let n_k = string_of_int (k+1) in
       "(_node_ "^(string_of_int id) ^ " " ^ c_k ^ " )","(_node_ "^(string_of_int id) ^ " " ^ n_k ^ " )"

     method print_nhtbl (label:string) =
       Hashtbl.iter( fun x y ->
                     Format.printf "Line %s :: %s => %a\n" label (self#nodeName y) (Bdd.print_minterm self#print_id) x) nodeHtbl


     (* Initial gamma function: From Bdd to SMT formula*)
     method initGamma (bddF : Cudd.Man.v Cudd.Bdd.t) =
       let idFalse = cnt#next in
       let idTrue = cnt#next in
       Hashtbl.replace nodeHtbl (Bdd.dfalse man) idFalse;
       Hashtbl.replace nodeHtbl (Bdd.dtrue man) idTrue;
       (*2 is the root node*)
       (2, [(idFalse,"false", "false");(idTrue,"true", "true")])


     method gamma (bddF:Cudd.Man.v Cudd.Bdd.t) (k:int)=
       let rec dfs (b: Cudd.Man.v Cudd.Bdd.t) (eLst:(int * string * string)list) (cK:int) =
         try
           let id = Hashtbl.find nodeHtbl bddF in
           (* debug_proc BDD_PROC None ("FOUND ID in DFS " ^ (self#nodeName id)); *)
           (id, eLst)
         with Not_found ->
              match Bdd.inspect b with
              | Bdd.Ite(x, lBdd, rBdd) ->
                 begin
                   let (lID, l_exprLst) = dfs lBdd eLst cK in
                   let (rID, r_exprLst) = dfs rBdd l_exprLst cK in
                   (* let id = self#nodeName cnt#next in *)
                   let id = cnt#next in
                   let pred, pred_bdd = self#safe_find bdd_store x "DFS" in
                   let k_str = string_of_int k in
                   let k_str_next = (string_of_int (k + 1)) in
                   let lK_cur, lK_next = self#mkNameK lID k in
                   let rK_cur, rK_next = self#mkNameK rID k in
                   let currentPredicate = "(ite (" ^ pred ^ " " ^ k_str ^ ") " ^ lK_cur ^ " " ^ rK_cur ^ ")" in
                   let nextPredicate = "(ite (" ^ pred ^ " " ^ k_str_next ^ ") " ^ lK_next ^ " " ^ rK_next ^ ")" in
                   let eq = (id, currentPredicate, nextPredicate) in
                   Hashtbl.add nodeHtbl pred_bdd id;
                   (id, eq::r_exprLst)

                 end
              | Bdd.Bool true ->
                 let id = Hashtbl.find nodeHtbl (Bdd.dtrue man) in
                 (* debug_proc BDD_PROC None ("FOUND ID of TRUE " ^ (self#nodeName id)); *)
                 (id, eLst)
              | Bdd.Bool false ->
                 let id = Hashtbl.find nodeHtbl (Bdd.dfalse man) in
                 (* debug_proc BDD_PROC None ("FOUND ID of FALSE " ^ (self#nodeName id)); *)
                 (id, eLst)
       in
       let exprLst = [(1,"false", "false");(2,"true", "true")] in
       dfs bddF exprLst k



     method ppInv (bddF : Cudd.Man.v Cudd.Bdd.t)  =
       let root, invLst =  self#pp bddF in
                   let streams = List.fold_right (fun (x,y,z,w) acc ->
                                                  let node =
                                                    match y with
                                                    | "false" -> "node_"^(string_of_int x)^ "= false;\n"
                                                    | "true" -> "node_"^(string_of_int x)^ "= true;\n"
                                                    | _ ->
                                                       "node_"^(string_of_int x)^" = if " ^ w ^ " then " ^ z ^ " else " ^ y ^ " ;\n";
                                                  in
                                                  node ^ acc) invLst "" in
                   let prop = "bdd = not (node_"^ (string_of_int root) ^" = true);\n" in
                   let vars = List.fold_right (fun (x,y,z,w) acc -> "node_"^(string_of_int x)^" :bool; "^acc) invLst "" in
                   let nodeOut =   "node bddInvariant (x:bool) returns (out:bool);\n "
                                   ^ "var " ^ vars ^ " bdd: bool;\n"
                                   ^ "let\n"
                                   ^ prop
                                   ^ streams
                                   ^ "--!PROPERTY : bdd;\n"
                                   ^ " tel\n" in
                   nodeOut

     method private pp  (bddF : Cudd.Man.v Cudd.Bdd.t) =
       let rec dfs b eLst =
         match Bdd.inspect b with
         | Bdd.Bool true ->
            let id =  Hashtbl.find nodeHtbl (Bdd.dtrue man) in
            (id, eLst)
         | Bdd.Bool false ->
            let id =  Hashtbl.find nodeHtbl (Bdd.dfalse man) in
            (id, eLst)

         | Bdd.Ite(x,lBdd, rBdd) ->
            begin
              try
                let (lID, l_exprLst) = dfs lBdd eLst  in
                let (rID, r_exprLst) = dfs rBdd l_exprLst in
                let pred, pred_bdd = self#safe_find bdd_store x "DFS" in
                            let pred_expr =  New_vars.nvr_to_expr pred in
                            let pred_str = Expr_util.map_org_il_expr pred_expr in
                            let id = cnt#next in
                            let lK_cur = "node_"^(string_of_int lID) in
                            let rK_cur = "node_"^(string_of_int rID) in
                            let eq = (id, lK_cur, rK_cur, pred_str) in
                            (id, eq::r_exprLst)
              with Not_found ->
                failwith "Not found ITE"
            end
       in
       let exprLst = [(1, "false", "false", "");(2, "true", "true", "")] in
       dfs bddF exprLst


     method mkNewCandidate (dfsF: (int * string * string)list) (k:int) =
       let currentBuff = Buffer.create 1000 in
       let nextBuff = Buffer.create 1000 in
                         let k_str = (string_of_int k) in
                         let k_str_next = (string_of_int (k+1)) in
                         let newCandDef =
                                 List.iter(fun (y,z,w) ->
                                         let id = self#nodeName y in
                                         let currentAss = "(assert (= (_node_ " ^ id ^ " " ^ k_str ^ ") " ^ z ^ "))\n" in
                                         let nextAss = "(assert (= (_node_ " ^ id ^ " " ^ k_str_next ^ ") " ^ w ^ "))\n" in
                                         Buffer.add_string currentBuff currentAss;
                                         Buffer.add_string nextBuff nextAss;
                                 ) dfsF in
                         Hashtbl.replace gammaBDD "current" (Buffer.contents currentBuff);
                         Hashtbl.replace gammaBDD "next" (Buffer.contents nextBuff);
                         currentBuff, nextBuff


                 method get_current_gBDD () = Hashtbl.find gammaBDD "current"

                 method get_next_gBDD () = Hashtbl.find gammaBDD "next"

                 method set_current_root newRootNode =
                         debug_proc BDD_PROC (None) ("Setting Current Root Node: " ^ (string_of_int newRootNode));
                         rootNode <- newRootNode

                 method get_current_root () = rootNode

                 method get_current_bdd () = current_bdd

                 method set_current_bdd new_bdd = current_bdd <- new_bdd

                 method get_next_bdd () = next_bdd

                 method set_next_bdd new_bdd = next_bdd <- new_bdd

                 method set_stable_bdd b = stable_bdd <- b

                 method get_stable_bdd () = stable_bdd

                 method get_nodeHtbl () = nodeHtbl

                 method isEqual (b1 : Cudd.Man.v Cudd.Bdd.t) (b2 : Cudd.Man.v Cudd.Bdd.t)= Bdd.is_equal b1 b2

                 method private print_id fmt id =
                         let (v,_) = self#safe_find bdd_store id "print_id" in
                         Format.pp_print_string fmt v

                 method print_bdd (bddF: Cudd.Man.v Cudd.Bdd.t) (tag: string)=
                         Format.printf " %s =>" tag;
                         Format.printf " %a  <= %s\n\n" (Bdd.print self#print_id) bddF tag

                 method print_bdd_htbl () =
                         Hashtbl.iter( fun x (y, z) ->
                                 Format.printf " %a\n" (Bdd.print_minterm self#print_id) z) bdd_store


         end
