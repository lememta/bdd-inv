(** BDD Graph *)

(** 
@author Temesghen Kahsai
*)
open Types
open Cudd
open Channels
open Printf

class ['a] bddGraph =

	let bdd = new Bdd_helper.bddBank in

	let cnt = new Counter.counter in

	let inv_cnt = new Counter.counter in

	let root = ref "" in
	
	object(self)
		
	  val mutable dotCollection = (Hashtbl.create 100:(int, string)Hashtbl.t)
					
	  val mutable invCollection = (Hashtbl.create 100:(int, (string * string * string * string)list)Hashtbl.t)

	  method private nodeName id = string_of_int id

	  method private mkNameK id = "node_"^id

	  method mkDot (dotBdd : (string * string * string * string)list) (meta:string)=
	    Hashtbl.add invCollection (inv_cnt#next) dotBdd; 
	    let (rootNode,_,_,_) = (List.hd dotBdd) in
	    root := rootNode;
	    let dotArg = List.fold_right (fun (x,y,z,w) acc ->
					  let node = 
					    match y with 
           				    | "false" -> "node_"^x ^ " [label=\"false\",shape=box,style=filled,color=\".7 .3 1.0\"]\n"
           				    | "true" -> "node_"^x ^ " [label=\"true\"shape=box,style=filled,color=\".7 .3 1.0\"]\n"
           				    | _ -> 
						     (*"node_"^x^" -> " ^ y ^ "[color=blue;  fontsize=5; arrowsize=0.4]\n"
                  ^"node_"^x^" -> " ^ z ^"[color=red; fontsize=5; arrowsize=0.4]\n"*)
					       if not (rootNode == x) then (
						 "node_"^x^" -> " ^ y ^ "[color=blue; label= \""^w^"\"; fontsize=5; arrowsize=0.4]\n"
           					 ^"node_"^x^" -> " ^ z ^"[color=red; label= \""^w^"\"; fontsize=5; arrowsize=0.4]\n")
					       else(
						 "node_"^x^ "[color=lightgrey; style=filled]\n" 
						 ^"node_"^x^ "-> " ^ y ^ "[color=blue; label= \""^w^"\"; fontsize=5; arrowsize=0.4]\n"
						 ^"node_"^x^ "  -> " ^ z ^"[color=red; label= \""^w^"\"; fontsize=5; arrowsize=0.4]\n")
           				  in
           				  node ^ acc) dotBdd "" in
	    let dotOut =   "subgraph "^meta
			   ^ " \n{\n node [fixedsize=true,width=.4; height=.2];color=lightgrey;label= \"" ^ meta ^"\"\n " 
			   ^ dotArg 
			   ^ " }\n" in
            Hashtbl.add dotCollection cnt#next dotOut
					       
       
	  method printDot () = 
	    output_string !bdd_dot "digraph bdd_dot{\n size=\"10,7\"; node[fontsize=6];\n";
	    Hashtbl.iter(fun x y -> output_string !bdd_dot y) dotCollection;
	    output_string !bdd_dot "}\n"; 
	    

	  method mkInvariant ()= 
	    let size = Hashtbl.length invCollection in
	    let size_checked = if size==1 then 1 else size in
	    let inv = Hashtbl.find invCollection size_checked in 
	    let rootLustre = "assert not (node_"^ !root ^" = true);" in
	    let streams = List.fold_right (fun (x,y,z,w) acc ->
					   let node = 
					     match y with 
					     | "false" -> "node_"^x^ "= false;\n"
					     | "true" -> "node_"^x^ "= true;\n"
					     | _ -> 
						"node_"^x^" = if " ^ w ^ " then " ^ z ^ " else " ^ y ^ " ;\n";
					   in
					   node ^ acc) inv "" in
	    let vars = List.fold_right (fun (x,y,z,w) acc -> "node_"^x^" :bool; "^acc) inv "" in
	    let nodeOut =   "node bddInvariant (x:bool) returns (out:bool);\n "
			    ^ "var " ^ vars ^ "\n"
			    ^ "let\n"
			    ^ rootLustre
			    ^ streams 
			    ^ " tel\n" in
            nodeOut
	      
	      
	  method gammaDot (bddF : Cudd.Man.v Cudd.Bdd.t) bddStore = 
	    let man = Bdd.manager bddF in
  	    let idTrue = self#nodeName cnt#next in
  	    let idFalse = self#nodeName cnt#next in
  	    let nodeHtbl = (Hashtbl.create 100:(Cudd.Man.v Cudd.Bdd.t, string)Hashtbl.t) in
  	    Hashtbl.add nodeHtbl (Bdd.dfalse man) idFalse;
  	    Hashtbl.add nodeHtbl (Bdd.dtrue man) idTrue;
  	    let expressionLst = [(idFalse, "false", "false", "");(idTrue,"true", "true", "")] in
  	    self#dot bddF nodeHtbl expressionLst man bddStore
  		     
  	  method private dot bddF nodeHtbl exprLst man bddStore= 
  	    let rec dfs b nHtbl eLst man bddStore = 
  	      match Bdd.inspect b with 
  	      | Bdd.Bool true -> 
  		 begin
  		   try
  		     let id = Hashtbl.find nHtbl (Bdd.dtrue man) in
  		     (id, eLst)
  		   with  Not_found -> 
  		     failwith "Not found True Leaf"
  		 end
  	      | Bdd.Bool false -> 
  		 begin
  		   try
  		     let id = Hashtbl.find nHtbl (Bdd.dfalse man) in
  		     (id, eLst)
  		   with  Not_found -> 
  		     failwith "Not found False Leaf"
  		 end
		   
  	      | Bdd.Ite(x,lBdd, rBdd) ->
  		 begin
  		   try
  		     let found_id = Hashtbl.find nHtbl b in 
  		     (found_id, eLst)
  		   with Not_found ->
  		     let (lID, l_exprLst) = dfs lBdd nHtbl eLst man bddStore in
  		     let (rID, r_exprLst) = dfs rBdd nHtbl l_exprLst man bddStore in
  		     let id = self#nodeName cnt#next in
  		     let pred, pred_bdd = bdd#safe_find bddStore x "DFS" in
  		     let pred_expr =  New_vars.nvr_to_expr pred in
  		     let pred_str = Expr_util.map_org_il_expr pred_expr in
  		     let lK_cur = self#mkNameK lID in
  		     let rK_cur = self#mkNameK rID in
		     
  		     let eq = (id, lK_cur, rK_cur, pred_str) in
  		     (id, eq::r_exprLst)
  		 end
  	    in
  	    dfs bddF nodeHtbl exprLst man bddStore
	   
	 method ppInv (bddF : Cudd.Man.v Cudd.Bdd.t)  = 
  	   let p, invLst =  self#pp bddF in 
	   let streams = List.fold_right (fun (x,y,z,w) acc ->
					  let node = 
					    match y with 
					    | "false" -> "node_"^(string_of_int x)^ "= false;\n"
					    | "true" -> "node_"^(string_of_int x)^ "= true;\n"
					    | _ -> 
					       "node_"^(string_of_int x)^" = if " ^ w ^ " then " ^ z ^ " else " ^ y ^ " ;\n";
					  in
					  node ^ acc) invLst "" in
           print_string streams
  		      
  	  method private pp bddF = 
  	    let rec dfs b eLst =
	      let nodeHtbl = bdd#get_nodeHtbl () in
  	      match Bdd.inspect b with 
  	      | Bdd.Bool true -> (2, eLst)
  		 
  	      | Bdd.Bool false ->  (1, eLst)
  	
  	      | Bdd.Ite(x,lBdd, rBdd) ->
  		 begin
  		   try
		     let (lID, l_exprLst) = dfs lBdd eLst  in
  		     let (rID, r_exprLst) = dfs rBdd l_exprLst in
		     let bddStore = bdd#get_bddStore () in
		     let pred, pred_bdd = bdd#safe_find bddStore x "DFS" in
		     let pred_expr =  New_vars.nvr_to_expr pred in
  		     let pred_str = Expr_util.map_org_il_expr pred_expr in
  		     let lK_cur = "node_"^(string_of_int lID) in
  		     let rK_cur = "node_"^(string_of_int rID) in
		     let eq = (x, lK_cur, rK_cur, pred_str) in
		      (x, eq::r_exprLst)
  		   with Not_found ->
  		     failwith "Not found ITE"
  		 end
  	    in
	     let exprLst = [(1, "false", "false", "");(2,"true", "true", "")] in
  	     dfs bddF exprLst



	end
	  
	  


