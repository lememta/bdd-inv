
(** Print the ASTs *)

(** {6 Functions} *)

(** Print out a {!Types.typed_stream} in human-readable format (for debugging) *)
val print_lustre_term : Types.typed_stream -> unit

(** Print out a {!Types.il_expression} in human-readable format (for debugging) *)
val print_expr_term : Types.il_expression -> unit

(** Print out a {!Types.il_formula} in human-readable format (for debugging) *)
val print_expr_formula : Types.il_formula -> unit

val print_node_defs : unit -> unit

val type_string : Types.lustre_type -> string

val expr_string : Types.il_expression -> string

val mk_property_string : Types.typed_stream -> string
