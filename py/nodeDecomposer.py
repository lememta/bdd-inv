from LogManager import LoggingManager
from lustreParser import LParser
from pyparsing import ParseException
from kindInterface import PkindJobs, KindUtil
from lustreNodes import BuildLusteNode
from ccNodes import CcNodes
import pprint
import config
import os
import xml.etree.ElementTree as ET

pp = pprint.PrettyPrinter(indent=4)


 
class BenchmarkJobs(object):
    """ Benchmarks main executor """
    def __init__(self):
        self._log = LoggingManager.get_logger(__name__)
        self.inv_options = config.INV_OPTIONS
        self.invBank = {}
        self.kindUtil = KindUtil()
        self.componentProps = None
        self.AllResults = {}
        return
 
    def verifyProps(self, lusFile, allNodes, timeout, inv, componentProps):
        self._log.info("Verifing %s", str(lusFile))
        self.componentProps = componentProps
        pkind = PkindJobs()
        pkind.PropertyResult = self.propResult
        node = allNodes[0]
        bddLustre = ["pkind", "-compression", "-with-inv-gen", node] if inv else ["pkind", "-compression", node]
        originalLustre = ["pkind", "-compression", "-with-inv-gen", lusFile] if inv else ["pkind", "-compression", lusFile]
        allCmds = [(originalLustre,lusFile), (bddLustre,node)]
        for cmd in allCmds:
            pkind.runJobs(cmd[1], cmd[1], cmd[0], timeout)
        self._log.info("Finished getting results.")
        print self.AllResults
        

    
    def propResult(self, result):
        self._log.debug("Results from KindBDD verification")
        try:
            self.parseXML(result)
        except Exception as e:
            self._log.exception(str(e))


    def parseXML(self, result):
        self._log.debug("Parsing XML Result")
        try:
            for node_name, r in result.iteritems():
                xmldoc = ET.fromstring(r)
                properties_tag = (xmldoc[0]).attrib['name']
                prop_list = properties_tag.split()
                bench_out = {}
                for p in properties_tag.split():
                    bench_out.update({"P": p, "K": xmldoc[0][2].text, "R": xmldoc[0][3].text, "T": xmldoc[0][1].text})
                    print bench_out
                self.AllResults.update({node_name:bench_out})
        except Exception as e:
            print e
            print "\t Error in verification"
            pass

   
            
class NodeComposer(object):
    """ Given a multi-node Lustre file it make the composition of nodes"""
    
    def __init__(self):
        self._log = LoggingManager.get_logger(__name__)
        self.LustreAST = {}
        self.kindUtil = KindUtil()
        self.d = None
        self.componentsNode = {}
        self.subrange = None
        return
    
    def parseFile(self, lusFile):
        parser = LParser()
        try:
            self.LustreAST= parser.parse(lusFile)
            self._log.debug(parser.ppAST(self.LustreAST))
            return True
        except ParseException, err:
            self._log.exception(str(err))
            return False

    def bddDecompose(self, lusFile, timeout):
        componentProps = {}
        multiNodes = {} # keep track of node calls inside a node
        anyPropNodes = {} # keep track if a node has propoerty to be verified
        bdd_invariants = None
        lustreFileBDD = None
        lusProgram = os.path.abspath(lusFile)
        cmd = ["kind_bdd", lusProgram]
        bdd_invariants = self.kindUtil.run_with_timeout(cmd, timeout)
        if bdd_invariants:
            with open(lusFile, 'r') as f:
                lustreFileBDD = "--@ ensures bddInvariant(true);\n"+ f.read() + "\n" +  bdd_invariants
            self._log.info("Parsing Lustre file: %s" % lusFile)
            if self.parseFile(lustreFileBDD):
                self._log.info("Successful parse")
                nodeNames = self.LustreAST.keys()
                mk_node = CcNodes(nodeNames)
                for n,d in self.LustreAST.iteritems():
                    if n == "glob":
                        pass
                    else:
                        node_original, node, prop_dict, extraNodes, anyProp, subrange = mk_node.ccLustre(d)
                        self.storeOriginalFile(node_original, lusFile)
                        self.subrange = subrange
                        if node is not None:
                            self.componentsNode.update({n:node})
                            componentProps.update({n:prop_dict})
                            multiNodes.update({n:extraNodes})
                            anyPropNodes.update({n:anyProp})
                        else:
                            self._log.exception("Building nodes")
                nodes = self.handleMultiNodes(multiNodes)

                return nodes, componentProps, multiNodes, anyPropNodes
            else:
                self._log.error("Parsing error : " + str(lusFile))
                return None, None, None, None
        else:
            self._log.error("Timeout generating BDD: " + str(lusFile))
            return None, None, None, None

    def handleSubrange(self, nodes):
        if self.parseFile(nodes):
            self._log.info("Successful parse")
            print self.LustreAST


    def handleMultiNodes(self, multiNodes):
        self._log.debug("Handling Multi Nodes")
        out_nodes = {}
        for n, nodes in self.componentsNode.iteritems():
            if (multiNodes[n] != []) and (multiNodes[n] != None):
                node = self.flattenNodes(nodes, multiNodes[n])
                out_nodes.update({n:node})
            else:
                out_nodes.update({n:nodes})
        return out_nodes


    def storeOriginalFile(self, node, lusFile):
        try:
            os.remove(lusFile)
            f = open(lusFile, "w+")
            f.write(node)
            f.close
        except OSError:
            self._log.error("Did not restore original File")


    def flattenNodes(self, mainNode, extraNodes):
        self._log.info("Flattening nodes")
        nodes = mainNode + "\n"
        for n in extraNodes:
            node = self.componentsNode[n]
            nodes += node
        flatnode = self.kindUtil.runLsimplify(nodes)
        return flatnode


    def storeNodes(self, lusDir, lustreNodes, componentProps, anyPropNodes):
        self._log.debug("Storing Nodes")
        allNodes = []
        compProps = {}
        self.d = ((lusDir.split("."))[0] + "_LUSTRE")
        if not os.path.exists(self.d):
            os.makedirs(self.d)
        for node_name, node in lustreNodes.iteritems():
            if node and anyPropNodes[node_name]:
                f_node = self.d+os.sep+node_name+".lus"
                allNodes.append(f_node)
                compProps.update({f_node: componentProps[node_name]})
                f = open(f_node, "w+")
                f.write(node)
                f.close
        return allNodes, compProps
            


class BddInv(object):
    def __init__(self, ):
        self._log = LoggingManager.get_logger(__name__)
        return
  
    def run(self, lusFile, timeout, inv):
        self._log.info("Running KBDD on:  " + str(lusFile))
        composer = NodeComposer()
        runner = BenchmarkJobs()
        nodes = None
        results = None
        try:
            nodes, componentProps, multiNodes, anyPropNodes = composer.bddDecompose(lusFile, timeout)
            if nodes and componentProps and multiNodes:
                 allNodes, compProps = composer.storeNodes(lusFile, nodes, componentProps, anyPropNodes)
                 results = runner.verifyProps(lusFile, allNodes, timeout, inv, compProps)
            else:
                return
            if results:
                print results
        except Exception as e:
            self._log.exception("Failed to run KBDD: " + str(e))




            
        

