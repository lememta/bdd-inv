import argparse
import textwrap
from LogManager import LoggingManager
from nodeDecomposer import BddInv
from evaluator import Eval

"""
TBD: Run template invariant generator add to the lustre file and then run Kind-Bdd
"""



if __name__ == "__main__":
    _log = LoggingManager.get_logger(__name__)
    parser = argparse.ArgumentParser(
        prog='Kind BDD',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
                  KIND BDD
            --------------------------------
            '''))
    parser.add_argument('-inv', '--invariants', default=False,  action="store_true", dest="inv")
    parser.add_argument('-v', '--version', action='version', version=('%(prog)s 0.1'))
    parser.add_argument('-f', '--file', required=False, dest="lustre_file" )
    parser.add_argument('-t', '--timeout', required=False, type=int, default=100, dest="timeout" )
    parser.add_argument('-l', '--list', required=False, dest="listFile" )
    parser.add_argument('-d', '--dir', required=False, dest="dir")
    args = parser.parse_args()
    inv = args.inv
    lustre_file = args.lustre_file
    timeout = args.timeout
    dirs = args.dir
    listFile = args.listFile

    if (lustre_file):
        _log.info("Single file")
        try:
            sa = BddInv()
            sa.run(lustre_file, timeout, inv)
        except Exception as e:
            _log.exception(str(e))
    elif (listFile and dirs):
        _log.info("Directory")
        ev = Eval(dirs, timeout, listFile, inv)
        ev.run()
    else:
        _log.exception("Choose between running benchmark in a dir or a single Lustre file")
