import argparse
import textwrap
import os
import subprocess
import xml.etree.ElementTree as ET
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import xlwt
from nodeDecomposer import BddInv


class Results(object):
    def __init__(self):
        return


    def parse(self, result, lusFile, timeout):
        print "Parsing XML"
	print result
        bench_out = {}
        xmldoc = ET.fromstring(result)
        for c in xmldoc.iter('Runtime'):
        	bench_out.update({"R": c.text, "T": c.attrib["timeout"]})
	bench_out.update({"A": xmldoc[0][3].text, "K": xmldoc[0][2].text})
        return {lusFile: bench_out}


class Eval(object):
    """docstring for Eval"""
    def __init__(self, dirs, timeout, listFile, inv):
        self.dir = dirs
        self.timeout = timeout
        self.listFile = listFile
        self.inv = inv
        self.res = Results()
        self.outYices = []
        self.outCvc4 = []
        
    def runDir(self):
        benchmark = os.listdir(self.dir)
        for bench in benchmark:
            resultYices = self.kind.runYices(self.dir+os.sep+bench)
            resultCvc4 = self.kind.runCvc4(self.dir+os.sep+bench)
            parsed_yices = self.res.parse(resultYices, bench, self.timeout)
            parsed_cvc4 = self.res.parse(resultCvc4, bench, self.timeout)
            if parsed_yices != {}:
                self.outYices.append(parsed_yices)
            if parsed_cvc4 != {}:
                self.outCvc4.append(parsed_cvc4)
        return

    def run(self):
        sa = BddInv()
        f = open(self.listFile, 'r')
        for bench in f.readlines():
            b_wn = bench.rstrip('\n')
            b = self.dir+b_wn+".ec"
            try:
                sa.run(b, self.timeout, self.inv)
            except:
                print "FAILED!!!!!"
            # resultYices = self.kind.runYices(b)
            # resultCvc4 = self.kind.runCvc4(b)
            # self.outYices.append(self.res.parse(resultYices, bench, self.timeout))
            # self.outCvc4.append(self.res.parse(resultCvc4, bench, self.timeout))
        # self.writeToFile()

    
    def writeToFile(self):
        print "Writing Excel file"
        workbook = xlwt.Workbook(encoding = 'ascii')
        ws = workbook.add_sheet('My Worksheet')
        ws.write(0, 0, label = 'Benchmark')
        ws.write(0, 1, label = 'Yices Runtime')
        ws.write(0, 2, label = 'CVC4 Runtime')
        ws.write(0, 3, label = 'Yices K iteration')
        ws.write(0, 4, label = 'CVC4 K iteration')
        row_cvc4 = 1
        col_cvc4 = 0
        for d_cvc4 in self.outCvc4:
            for bench, data in d_cvc4.iteritems():
                ws.write(row_cvc4, col_cvc4, label = str(bench))
                ws.write(row_cvc4, (col_cvc4+2), str(data['R']))
                ws.write(row_cvc4, (col_cvc4+4), str(data['K']))
            row_cvc4 +=1
        row_yices = 1
        col_yices = 0
        for d_yices in self.outYices:
            for bench, data in d_yices.iteritems():
                #ws.write(row_yices, col_yices, label = str(bench))
                ws.write(row_yices, (col_yices+1), str(data['R']))
                ws.write(row_yices, (col_yices+3), str(data['K']))
            row_yices +=1
        workbook.save(self.listFile+'_results.xls')


    def plot(self):
        print "Plotting"
        runtime_yices = []
        runtime_cvc4 = []
        for bench in self.outYices:
            for lus, d in bench.iteritems():
                runtime_yices.append(d['R'])
        for bench in self.outCvc4:
            for lus, d in bench.iteritems():
                runtime_cvc4.append(d['R'])
	    print runtime_yices
	    print runtime_cvc4
        plt.plot(runtime_yices, runtime_cvc4, 'ro')
        plt.ylabel('Pkind - CVC4')
        plt.xlabel('Pkind - Yices')
        plt.savefig('plot.png')
        return
        

        


