
import time
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
from LogManager import LoggingManager
import threading
 

class KindUtil(object):
    def __init__(self):
        self._log = LoggingManager.get_logger(__name__)
        return

    def runLsimplify(self, lusFile):
        self._log.debug("Running Lsimplify")
        cmd = ["lsimplify", "-s", "-nm", lusFile]
        p = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        result, _ = p.communicate()
        return result


    def run_with_timeout(self, command, timeout):
        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        while timeout > 0:
            if p.poll() is not None:
                result, _ = p.communicate()
                return result
            time.sleep(0.1)
            timeout -= 0.1
        else:
            try:
                p.kill()
            except OSError as e:
                if e.errno != 3:
                    raise
        return None


class PkindJobs(threading.Thread):
    
    _active_tasks = []
    
    def __init__(self):
        self._log = LoggingManager.get_logger(__name__)
        return
    
    @property
    def all_tasks_done(self):
        return len(self.active_tasks) <=0

    @property
    def active_tasks(self):
        return self._active_tasks


    def GetInvariants(self, inv):
        self._log.exception("GetInvariants is NOT overridden")
        pass

    def PropertyResult(self, propResult):
        self._log.exception("Property result is NOT overridden")
        self._propDone = True
        pass
       
    def WaitAllTasksDone(self, timeout=None):
        start_time = time.time()
        while self.all_tasks_done is False and True if timeout is None else start_time + timeout > time.time():
            time.sleep(1)
     
    @classmethod
    def _add_task(cls, task):
        cls._active_tasks.append(task)

    @classmethod
    def _remove_task(cls, task):
        cls._active_tasks.remove(task)
         
    def runJobs(self, job_id, node_name, job, timeout):
        self._log.info("Running Pkind : %s", job_id)
        th = PkindThread(self, job_id, node_name, job, timeout)
        self._add_task(th)
        th.start()
        th.join(timeout)
        if th.is_alive():
            self._log.info("TIMEOUT for : %s" % th.getName())
            th.p.terminate()
            th.join()
        
    
        

class PkindThread(threading.Thread):
    
    def __init__(self, pkind, job_id, node_name, cmd, timeout):
        self.cmd = cmd
        self.pkind = pkind
        self.timeout = timeout
        self.node_name = node_name
        self.p = None
        super(PkindThread, self).__init__()
        self.setName(job_id)
        return
        
    def task_done(self):
        PkindJobs._remove_task(self)
    

    def run(self):
        self.p = subprocess.Popen(self.cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        result, _ = self.p.communicate()
        print result
        if "</Property>" in result:
            self.pkind.PropertyResult({self.node_name : result})
        elif "error" in result:
            error = "<Error> \n" + str(result) + "</Error>"
            self.pkind.PropertyResult({self.node_name: error})
        return
                
